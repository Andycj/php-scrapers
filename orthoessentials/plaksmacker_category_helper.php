<?php
function parse_category($html  , $task){
    $out = array();
    $out['products'] = parse_products($html , $task);
    $out['next_page'] = parse_next_page($html , $task);
    return $out;
}
function parse_products($html , $task){
    $out = array();
    $product_url = json_decode($html,true);
    foreach($product_url['products'] as $pos_product){
        $out[] = "https://www.plaksmacker.com/api/v1/products/".$pos_product['id']."?expand=documents,specifications,styledproducts,htmlcontent,attributes,crosssells,pricing";
    }
    $out = array_unique($out);
//    $myfile = fopen("/home/dent/public_html/newdev/productinsert.txt", "w") or die("Unable to open file!");
//    fwrite($myfile, print_r($out,1));
//    fclose($myfile); 
    return $out;
}
function parse_next_page($html , $task){
    $product_url = json_decode($html,true);
    if($product_url['pagination']['nextPageUri'] != ""){
        $queryStr_data = plak_get_next_page($product_url['pagination']['nextPageUri']);
        if($queryStr_data){	
            return $queryStr_data;
        }	
    }
return false;
}
function plak_get_next_page($queryStr){
   if($queryStr != ""){
	   return $queryStr;
   }
    return false;
}

