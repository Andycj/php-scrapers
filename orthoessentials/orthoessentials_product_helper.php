<?php

function mspro_orthoessentials_title($html){
    $title = '';
    $instruction = 'h1';
    $parser = new nokogiri($html);
    $data = $parser->get($instruction)->toArray();
    unset($parser);
    if (isset($data[0]['#text'])) {
        $title =  $data[0]['#text'];
        //echo $title."</br>";
        return $title;
    }
return '';
}

function mspro_orthoessentials_manufacture($html){
    return 'Orthoessentials';
    //echo "Orthoessentials"."</br>";
}



function mspro_orthoessentials_description($html){
    $res = '';
    $pq = phpQuery::newDocumentHTML($html);
    $text  = $pq->find('div.product-description.product-page-block div');
    $res .=$text->html();
    //echo $res.'</br>';
    return $res;
}


function mspro_orthoessentials_price($html){
    $price = '';
    $str = explode('var product_price = ', $html);
    if (isset($str)){
        $price = strtok($str[1], ';');
    //echo print_r($price, 1).'<br>';
        return $price;
    }
return '';	
}


function mspro_orthoessentials_special_price($html){
return '';	
}

function mspro_orthoessentials_sku($html){
    return mspro_orthoessentials_model($html);
}

function mspro_orthoessentials_model($html){
    $res = '';
    $instruction = 'div.product-id';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    unset ($parser);
    if($res){
        $data = $res[0]['#text'];
        $ares = explode(":", $data);
        $area = explode('&', $ares[1]);
        $res = $area[0];
        return $res;
        //echo $res.'</br>';
    }
return '';
}

function mspro_orthoessentials_meta_description($html){
    $meta = '';
    $res =  explode('<meta name="twitter:description" content="Features:', $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
            $metas = $res[0];
            $meta = preg_replace('#\s+#', ',', trim($metas));
            return $meta;
            //echo '<pre>'.print_r($meta, 1).'<pre>';
        }
    }
return '';
}

function mspro_orthoessentials_meta_keywords($html){
    $res = '';
    $res =  explode('<meta name="twitter:description" content="Features:', $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
            $resa = $res[0];
            $res = preg_replace('#\s+#', '-', trim($resa));
            return $res;
            //echo '<pre>'.print_r($res, 1).'<pre>';
        }
    }
return mspro_orthoessentials_title($html);
}


function mspro_orthoessentials_main_image($html){
    $arr = '';
    $arr = mspro_orthoessentials_get_images_arr($html);
    if(isset($arr[0]) && strlen($arr[0]) > 0){
        return $arr[0];
    }
return '';
}


function mspro_orthoessentials_other_images($html){
    $arr = '';
    $arr = mspro_orthoessentials_get_images_arr($html);
    if(count($arr) > 1){
        unset($arr[0]);
        return $arr;
    }
return '';
}

Function mspro_orthoessentials_get_images_arr($html){
    $out = '';
    $out = array();
    $instruction = 'div.gap-left a';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach ($res as $img) {
            $outs = $img['href'];
            if (strpos($outs, 'images') !== false){
                $out[] = 'http://www.orthoessentials.net/'.$outs;
            }
            $out = array_unique($out);
        }
        return $out;
    }
return '';   
}


function mspro_orthoessentials_options($html){
    $OPTION = array();
    $out = array();
    $instruction = '#content';
    $parser = new nokogiri($data);
    $log_html = $parser->get($instruction)->toHTML();

    $scr = preg_match('|var inventory_hashes = (.*)}];|', $log_html, $scripts);
    $scr = $scripts[0];
    if (isset($scr)){
        $a = str_replace('var inventory_hashes = ', '', $scr);
        $scr = str_replace(';', '', $a);

        $scripts = json_decode($scr, True);
        if (isset($scripts)){
            $OPTION = array();
            $OPTION['name'] = 'Available Options';
            $OPTION['type'] = "select";
            $OPTION['required'] = true;
            $OPTION['values'] = array();
            foreach($scripts as $option_value){
                if(isset($option_value)){
                    $OPTION['values'][] = array('name' => $option_value['attributes_list'] , 'price' => 0);
                }
            }
            if(count($OPTION['values']) > 0){
                    $out[] = $OPTION;
            }
            return $out;
        }
    }
return '';
}


function mspro_orthoessentials_noMoreAvailable($html){
    return true;
}