<?php
$config['additionalmarkets'][] = array(
    "name" => "orthoessentials",
    "title" => "<b>orthoessentials.net</b>-Scraper with login",
    "url_aliases" => array(
        "orthoessentials.net" , 
        "orthoessentials"),
    "fields" => array(
        "title", 
        "description")
    );
?>