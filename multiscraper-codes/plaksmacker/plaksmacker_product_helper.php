<?php
function mspro_plaksmacker_title($html){
	
	$title = json_decode($html,true);	
	if (isset($title['product']['shortDescription'])) {
	 	
			$title_name =  trim(str_replace(array("&nbsp;" , "&amp;","®","™") , array(" " , "`" ) ,$title['product']['shortDescription']));
			return $title_name;
    }  
    return '';
}

function mspro_plaksmacker_manufacture($html){
	
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	 	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text']));
    }
 	if (isset($data[0]['#text'][0]) && !is_array($data[0]['#text'][0])) {
	   	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text'][0]));
    }   
    return '';
}



function mspro_plaksmacker_description($html){
		$des_text ="";
		$description = json_decode($html,true);

		foreach($description['product']['specifications'] as $des){
				$des_text .= $des['htmlContent']."<br>";
	
		}
		
			return $des_text;

}


function mspro_plaksmacker_price($html){
    
		$price = json_decode($html,true);	
		$price_val = $price['product']['pricing']['regularPrice'];
		return (float) $price_val;
	
	
}

function mspro_plaksmacker_special_price($html){
    
	$instruction = 'span.pricing';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	    $special_price = preg_replace("/[^0-9.]/", "",  trim($data[1]['#text']) );
		return (float) $special_price;
		
	}
	return '';
	
}


function mspro_plaksmacker_sku($html){
    return mspro_plaksmacker_model($html);
}

function mspro_plaksmacker_model($html){
	
		$sku = json_decode($html,true);
		return $sku_val =  trim($sku['product']['sku']);
}


function mspro_plaksmacker_meta_description($html){
        $res =  explode('<meta name="description" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "plaksmacker") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
       return '';
}

function mspro_plaksmacker_meta_keywords($html){
     
       $keyword =  mspro_plaksmacker_title($html);
	    $second = str_replace(',','',$keyword);
	    $third = str_replace(' ',',',$second);
		$forth = str_replace('-',',',$third);
	   return $forth;
}


function mspro_plaksmacker_main_image($html){
		$image = json_decode($html,true);
		return $image['product']['largeImagePath'];
}


function mspro_plaksmacker_other_images($html){
	$arr = mspro_plaksmacker_get_images_arr($html);
	if(count($arr) > 1){
		unset($arr[0]);
		return $arr;
	}
	return array();
}


function mspro_plaksmacker_get_images_arr($html){
       /* $out = array();
        $image = json_decode($html,true);
		$out[0] = $image['product']['largeImagePath'];
		
		 $myfile = fopen("/home/demodenta/public_html/image.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $out);
		fclose($myfile); */
		
		return '';
		
}




function mspro_plaksmacker_options($html){
    $out = array();
	$opt_val =json_decode($html,true);
        foreach($opt_val['product']['styleTraits'] as $POS_option){
                $OPTION = array();
                $OPTION['name'] = trim(str_replace( array(":") , array("") , $POS_option['name']));
                $OPTION['type'] = "select";
                $OPTION['required'] = true;
                $OPTION['values'] = array();
                foreach($POS_option['styleValues'] as $option_value){
                        $OPTION['values'][] = array('name' => $option_value['valueDisplay'] , 'price' => 0);
                }
                if(count($OPTION['values']) > 0){
                    $out[] = $OPTION;
                }
            
        }
		return $out;
}


function mspro_plaksmacker_noMoreAvailable($html){
    if(!stripos($html , '<span>In stock</span>') > 0){
        return true;
    }
	return false;
}
