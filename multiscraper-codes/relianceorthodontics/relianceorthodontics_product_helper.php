<?php


function mspro_relianceorthodontics_title($html){
    $instruction = 'span[itemprop=name]';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	 	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text']));
    }
    if (isset($data[1]['#text']) && !is_array($data[1]['#text'])) {
        return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[1]['#text']));
    }
 	if (isset($data[0]['#text'][0]) && !is_array($data[0]['#text'][0])) {
	   	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text'][0]));
    }   
    return '';
}

function mspro_relianceorthodontics_description($html){
	$res = '';
		
	$pq = phpQuery::newDocumentHTML($html);

	$temp  = $pq->find('div#ProductDetail_ProductDetails_div');
	foreach ($temp as $block){
	   $res .= $temp->html();
	}
	
	//$res = str_ireplace(array("1-866-346-5665"), array("1-800-833-7132") , $res);
	
	/*$res = preg_replace('/<img(?:\\s[^<>]*)?>/i', '', $res);*/
	return $res;
}


function mspro_relianceorthodontics_price($html){
    $instruction = 'span[itemprop=price]';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	    $price = preg_replace("/[^0-9.]/", "",  trim($data[0]['#text']) );
	    return (float) $price;
	}
	if (isset($data[0]['#text'][1]) && !is_array($data[0]['#text'][1])) {
	    $price = preg_replace("/[^0-9.]/", "",  trim($data[0]['#text'][1]) );
	    return (float) $price;
	}
    return '';
}


function mspro_relianceorthodontics_sku($html){
    return mspro_relianceorthodontics_model($html);
}

function mspro_relianceorthodontics_model($html){
    $res = explode('<span class="product_code">' , $html);
    if(count($res) > 1){
        $res = explode('<' , $res[1] , 2);
        if(count($res) > 1){
            return trim($res[0]);
        }
    }
    return '';
}


function mspro_relianceorthodontics_meta_description($html){
        $res =  explode('<meta name="description" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "relianceorthodontics") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
       return '';
}

function mspro_relianceorthodontics_meta_keywords($html){
        $res =  explode('<meta name="keywords" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "relianceorthodontics") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
       return mspro_relianceorthodontics_title($html);
}


function mspro_relianceorthodontics_main_image($html){
	$arr = mspro_relianceorthodontics_get_images_arr($html);
	if(isset($arr[0]) && strlen($arr[0]) > 0){
		return $arr[0];
	}
	return '';
}


function mspro_relianceorthodontics_other_images($html){
	$arr = mspro_relianceorthodontics_get_images_arr($html);
	if(count($arr) > 1){
		unset($arr[0]);
		return $arr;
	}
	return array();
}


function mspro_relianceorthodontics_get_images_arr($html){
        $out = array();
        
        $res = explode('<meta property="og:image" content="' , $html);
        if(count($res) > 1){
            $res = explode('"' , $res[1] , 2);
            if(count($res) > 1){
                $out[] = trim($res[0]);
            }
        }
        
        foreach($out as $k => $v){
            $out[$k] = str_replace(array(" " , "2T.") , array("%20" , "2."), $v);
        }
        
        $out = array_unique($out);
        //echo '<pre>'.print_r($out , 1).'</pre>';exit;
        
        return $out;
}




function mspro_relianceorthodontics_options($html){
    $out = array();
    
    $options_NAMES = array();
    $instruction = 'table#options_table i b';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $pos_option_name){
            if(isset($pos_option_name['#text']) && !is_array($pos_option_name['#text']) ){
                $options_NAMES[] = $pos_option_name['#text'];
            }
        }
    }

    $instruction = 'table#options_table select';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
    unset($parser);
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $key => $POS_option){
            if(isset($POS_option['option']) && is_array($POS_option['option']) && count($POS_option['option']) > 0 && isset($options_NAMES[$key]) ){
                $OPTION = array();
                $OPTION['name'] = trim(str_replace( array(":") , array("") , $options_NAMES[$key]));
                $OPTION['type'] = "select";
                $OPTION['required'] = true;
                $OPTION['values'] = array();
                foreach($POS_option['option'] as $option_value){
                    if(isset($option_value['#text']) && !is_array($option_value['#text']) && strlen(trim($option_value['#text'])) > 0 ){
                        $OPTION['values'][] = array('name' => $option_value['#text'] , 'price' => 0);
                    }
                }
                if(count($OPTION['values']) > 0){
                    $out[] = $OPTION;
                }
            }
        }
    }
	/* $myfile = fopen("/home/demodenta/public_html/newfile.txt", "w") or die("Unable to open file!");
	fwrite($myfile, $out);
	fclose($myfile); */
    //echo '<pre>'.print_r($out , 1).'</pre>';exit;
    
    return $out;
}


function mspro_relianceorthodontics_noMoreAvailable($html){
    if(!stripos($html , '<span>In stock</span>') > 0){
        return true;
    }
	return false;
}
