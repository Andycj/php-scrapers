<?php
$config['additionalmarkets'][] = array(
    "name" => "orthoessentials",
    "title" => "<b>Orthoessentials.net</b> - Scraper with LOGIN",
    "url_aliases" => array(
        "orthoessentials.net" , 
        "www.orthoessentials.net",
		"orthoessentials"),
    "fields" => array(
        "title", 
        "description")
    );
?>