<?php
function parse_category($html, $task){
    $out = array();
    $out['products'] = parse_products($html , $task);
    $out['next_page'] = parse_next_page($html , $task);
    return $out;
}        
        
function parse_products($html, $task){
    
    $out = array();
    $instruction = 'div.catalog-product-title a';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    unset($parser);
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $pos_product){
            if(isset($pos_product['href']) && !is_array($pos_product['href']) && strlen($pos_product['href']) > 0){
                $out[] = $pos_product['href'];
            }
        }
        $out = array_unique($out);
    }
return $out;
}

function parse_next_page($html , $task){

    $instruction = 'div.catalog-page-breaks span a';
    $parser = new nokogiri($html);
    $count = $parser->get($instruction)->toArray();
    unset($parser);
    if(isset($count) && is_array($count) && count($count) > 0){
        foreach($count as $con){
            if (isset($con['href']) && !is_array($con['href']) && strlen($con['href']) > 0){
                $queryStr_data = orthoessentials_get_next_page($con['href']);
                if($queryStr_data){
                    array_pop($queryStr_data);
                    return $queryStr_data;
                }	
            }
        }   
    }       
return false;
}
function orthoessentials_get_next_page($queryStr){
    if($queryStr != ""){
	return $queryStr;
    }
return false;
}
?>