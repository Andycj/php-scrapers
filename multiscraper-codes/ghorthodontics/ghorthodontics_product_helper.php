<?php
function mspro_ghorthodontics_title($html){
    $instruction = '#labelDescription';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	 	return trim(str_replace(array("&nbsp;" , "&amp;","&reg;","&trade;","™") , array(" " , "`"),$data[0]['#text']));
    }
 	if (isset($data[0]['#text'][0]) && !is_array($data[0]['#text'][0])) {
	   	return trim(str_replace(array("&nbsp;" , "&amp;","&reg;","&trade;") , array(" " , "`") ,$data[0]['#text'][0]));
    }   
    return '';
}

function mspro_ghorthodontics_manufacture($html){
    $instruction = 'h1';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	 	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text']));
    }
 	if (isset($data[0]['#text'][0]) && !is_array($data[0]['#text'][0])) {
	   	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text'][0]));
    }   
    return '';
}



function mspro_ghorthodontics_description($html){
	$res = '';
	
	$pq = phpQuery::newDocumentHTML($html);
	$temp  = $pq->find('.col-md-7');
		
		foreach ($temp as $block){
		$res .=  $temp->html() . '<br />';
	}
		 $res1 =  explode('_ctl0__ctl4_labelDescriptionLabel' , $res);
		 
		$data_val = str_replace('<div id="_ctl0__ctl4_divTabs" class="tabs">','',trim($res1['2']));
		$data_val1 = str_replace('<div class="clearfix">','',$data_val);
		$data_val2 = str_replace('</div>','',$data_val1);
		$data_val3 = str_replace('',' ',$data_val2);
		$data_val4 = str_replace('<nav class="tabheader">',' ',$data_val3);
		$data_val5 = str_replace('</nav>',' ',$data_val4);
		$data_val6 = str_replace('<div id="topicb" class="content">',' ',$data_val5);
		$data_val7 = str_replace('<!-- Documentation -->',' ',$data_val6);
		$data_val8 = str_replace('<!-- BEGIN TAB AREA -->',' ',$data_val7);
		$data_val9 = str_replace('<!-- BEGIN ItemDetailTabs.ascx -->',' ',$data_val8);
		$data_val10 = str_replace('<!-- END ItemDetailTabs.ascx -->',' ',$data_val9);
		$data_val11 = str_replace('">',' ',$data_val10);
		
		
			/*$myfile = fopen("/home/dent/public_html/newdev/des1.txt", "w") or die("Unable to open file!");
			fwrite($myfile, print_r($data_val5,1));
			fclose($myfile);
	 */
	 
	 
			
		
		return $data_val11 ;
}


function mspro_ghorthodontics_price($html){
    
	$instruction = 'span.pricing';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	    $price = preg_replace("/[^0-9.]/", "",  trim($data[0]['#text']) );
	    return (float) $price;
	}
	return '';
	
}

function mspro_ghorthodontics_special_price($html){
    
	$instruction = 'span.pricing';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	    $special_price = preg_replace("/[^0-9.]/", "",  trim($data[1]['#text']) );
		return (float) $special_price;
		
	}
	return '';
	
}


function mspro_ghorthodontics_sku($html){
    return mspro_ghorthodontics_model($html);
}

function mspro_ghorthodontics_model($html){
    $instruction = '#item-detail .margin-bottom-mini span';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
		
	if($res){
		$sku = explode("#",$res[1]['#text']);
		return $sku['1'];
	}
	return '';
}


function mspro_ghorthodontics_meta_description($html){
        $res =  explode('<meta name="description" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "ghorthodontics") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
       return '';
}

function mspro_ghorthodontics_meta_keywords($html){
        $res =  explode('<meta name="keywords" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "ghorthodontics") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
	   $meta_keyword = mspro_ghorthodontics_title($html);
	  $mt = str_replace(array(' ','-',' - ',', '),',',$meta_keyword);
	  
	  
 $first = str_replace(' - ',',',$mt);

 $second = str_replace(' ',',',$first);

 $third = str_replace('-',',',$second);

  $data =  str_replace(',,',',',$third); 
	  
	
	  return $data;
}


function mspro_ghorthodontics_main_image($html){

		$arr = mspro_ghorthodontics_get_images_arr($html);
    if(isset($arr[0]) && strlen($arr[0]) > 0){
        return $arr[0];
    }
	return '';
	
}


function mspro_ghorthodontics_other_images($html){
	$arr = mspro_ghorthodontics_get_images_arr($html);
	
	$myfile = fopen("/home/dent/public_html/newdev/image_last.txt", "w") or die("Unable to open file!");
	fwrite($myfile, print_r($arr,1));
	fclose($myfile); 

	
	
	
	if(count($arr) > 1){
		unset($arr[0]);
		return $arr;
	}
	return array();
}


function mspro_ghorthodontics_get_images_arr($html){
    $out = array();
    //$instruction = '.col-md-7 .well img';
	$instruction = '#item-detail .col-md-7 img';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
	
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach ($res as $img) {
            $out[] = $img['src'];
        }
    $out = array_unique($out);
    return $out; 
    } 
}




function mspro_ghorthodontics_noMoreAvailable($html){
    if(!stripos($html , '<span>In stock</span>') > 0){
        return true;
    }
	return false;
}
