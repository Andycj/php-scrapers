<?php
function mspro_dcdental_title($html){
    $instruction = '.product-details-full-main-content-right .product-details-full-content-header-title';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);	 	
	
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	 	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text']));
    }
 	if (isset($data[0]['#text'][0]) && !is_array($data[0]['#text'][0])) {
	   	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text'][0]));
    }   
    return '';
}

function mspro_dcdental_manufacture($html){
    $instruction = '.product-details-full-main-content-right .product-details-main-content-manufacturer';
	$parser = new nokogiri($html);
	$data1 = $parser->get($instruction)->toArray();
	unset($parser);
	
	if (isset($data1[0]['#text']) && !is_array($data1[0]['#text'])) {
	 	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data1[0]['#text']));
    }
 	if (isset($data1[0]['#text'][0]) && !is_array($data1[0]['#text'][0])) {
	   	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data1[0]['#text'][0]));
    }   
    return '';
}



function mspro_dcdental_description($html){
	$res = '';
	$res1 = '';
	 
	$pq = phpQuery::newDocumentHTML($html);
	$pq1 = phpQuery::newDocumentHTML($html);

	$temp  = $pq->find('#product-details-information-tab-content-container-0');
	foreach ($temp as $block){
		$res .=  $temp->html() . '<br />';
	}
	/*$res = preg_replace('/<img(?:\\s[^<>]*)?>/i', '', $res);*/
	
	$temp1  = $pq1->find('#product-details-information-tab-1 #product-details-information-tab-content-container-1');
	foreach ($temp1 as $block){
		$res1 .=  $temp1->html() . '<br />';
	}
	return $data =  $res."<br>".$res1;
}

function mspro_dcdental_price($html){
    
	$instruction = 'span.pricing';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	    $price = preg_replace("/[^0-9.]/", "",  trim($data[0]['#text']) );
	    return (float) $price;
	}
	return '';
	
}

function mspro_dcdental_special_price($html){
    
	$instruction = 'span.pricing';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	    $special_price = preg_replace("/[^0-9.]/", "",  trim($data[1]['#text']) );
		return (float) $special_price;
		
	}
	return '';
	
}


function mspro_dcdental_sku($html){
    return mspro_dcdental_model($html);
}

function mspro_dcdental_model($html){
    $instruction = '.product-details-full-main-content-right .product-line-sku-value';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();

	if($res){
		return trim($res[0]['#text']);
	}
	return '';
}


function mspro_dcdental_meta_description($html){
        $res =  explode('<meta name="description" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "dcdental") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
       return '';
}

function mspro_dcdental_meta_keywords($html){
        $res =  explode('<meta name="keywords" content="' , $html);
		
		
		
		
       if(count($res) > 1)
	   {
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1)
			{
				if($res[0] != "" || empty($res[0]))
				{
					$mt = mspro_dcdental_title($html);
				}else
				{
					return str_replace(array("&nbsp;" , "&amp;" , "dcdental") , array(" " , "`" , "") , $res[0]);	
				}
       		}
       		 
		}	  
			//$mt = mspro_dcdental_title($html);
		
			$second = str_replace(' ',',',$mt);
			$third = str_replace('-',',',$second);
			$data =  str_replace(',,',',',$third); 
			
				$myfile = fopen("/home/dent/public_html/newdev/comma.txt", "a") or die("Unable to open file!");
							fwrite($myfile,print_r($data,1));
							fclose($myfile);
	   
       return $data;
}


function mspro_dcdental_main_image($html){
	$arr = mspro_dcdental_get_images_arr($html);
	if(isset($arr[0]) && strlen($arr[0]) > 0){
		return $arr[0];
	}
	return '';
}


function mspro_dcdental_other_images($html){
	$arr = mspro_dcdental_get_images_arr($html);
	if(count($arr) > 1){
		unset($arr[0]);
		return $arr;
	}
	return array();
}


function mspro_dcdental_get_images_arr($html){
        $out = array();
        $instruction = '.product-details-image-gallery-detailed-image img';
        $parser = new nokogiri($html);
		$res = $parser->get($instruction)->toArray();
		foreach ($res as $img)
		{
					$image_name = explode("?",$img['src']);
				$out[] = $image_name['0'];
		}		
        $out = array_unique($out);
		return $out;
}



/*
function mspro_dcdental_options($html){
    $out = array();

   /*  $instruction = 'div.item-selection-wrapper select';
    $out = array();
     
    $options_NAMES = array();
    $instruction1 = 'div.item-selection-wrapper select';
    $parser1 = new nokogiri($html);
    $res1 = $parser1->get($instruction1)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
	if(isset($res1) && is_array($res1) && count($res1) > 0){
        foreach($res1 as $pos_option_name){
            if(isset($pos_option_name['option']) && count($pos_option_name['option']) > 0){
				$options_NAMES[] = str_replace("-"," ",$pos_option_name['option'][0]['#text']);
            }
        }
    }
	
    $instruction = 'div.item-selection-wrapper select';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
    unset($parser);
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $key => $POS_option){
            if(isset($POS_option['option']) && is_array($POS_option['option']) && count($POS_option['option']) > 0){
				array_shift($POS_option['option']);

                $OPTION = array();
                $OPTION['name'] = trim(str_replace( array(":") , array("") , $options_NAMES[$key]));
                $OPTION['type'] = "select";
                $OPTION['required'] = true;
                $OPTION['values'] = array();
                foreach($POS_option['option'] as $option_value){
                    if(isset($option_value['#text']) && !is_array($option_value['#text']) && strlen(trim($option_value['#text'])) > 0 ){
                        $OPTION['values'][] = array('name' => $option_value['#text'] , 'price' => 0);
                    }
                }
                if(count($OPTION['values']) > 0){
                    $out[] = $OPTION;
                }
            }
        }
    }
	/* $myfile = fopen("/home/demodenta/public_html/newfile.txt", "w") or die("Unable to open file!");
	fwrite($myfile, $out);
	fclose($myfile); 
    //echo '<pre>'.print_r($out , 1).'</pre>';exit;
    
    return $out;
}
*/

function mspro_dcdental_noMoreAvailable($html){
    if(!stripos($html , '<span>In stock</span>') > 0){
        return true;
    }
	return false;
}
