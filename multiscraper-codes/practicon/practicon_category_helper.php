<?php
function parse_category($html  , $task){
		$out = array();
		$out['products'] = parse_products($html , $task);
		$out['next_page'] = parse_next_page($html , $task);
		return $out;
}
function parse_products($html , $task){
	//echo $html;exit;
		$out = array();
	
		
		$instruction = 'div.QC1navigationitems a';
        $parser = new nokogiri($html);
        $res = $parser->get($instruction)->toArray();
		 
        //echo '<pre>'.print_r($res , 1).'</pre>';exit;
        if(isset($res) && is_array($res) && count($res) > 0){
            foreach($res as $pos_product){
				if(isset($pos_product['href']) && !is_array($pos_product['href']) && strlen($pos_product['href']) > 0 ){
                    if(strpos($pos_product['href'] , "practicon.") > 0){
                        $out[] = $pos_product['href'];
					}else{
                        $out[] = 'http://www.practicon.com' . $pos_product['href'];
					}   
                }
            }
        
        }
        
        $out = array_unique($out);
        //echo '<pre>'.print_r($out , 1).'</pre>';exit;
        
        return $out;
}
function parse_next_page($html , $task){
        // check if isset nest page
       // if(stripos($html , '<a href="javascript:RedirectToNavigation') > 0){
            // get base
            $res = explode('<link rel="canonical" href="' , $html);
			
			$instruction = 'div.pagingcss a';
			$parser = new nokogiri($html);
			$html_res = $parser->get($instruction)->toArray();
			
			if(count($res) > 1){
                $res = explode('"' , $res[1] , 2);
                if(count($res) > 1){
                    $base = trim($res[0]);
                    // get query string
                    $res = explode("var SearchParams = '" , $html);
                    if(count($res) > 1){
                        $res = explode("'" , $res[1] , 2);
                        if(count($res) > 1){
                            $queryStr = trim($res[0]);
                            $queryStr = practicon_get_next_page($queryStr);
                            if($queryStr){
                                //echo $base . '?' . $queryStr;exit;
                                return $base . '?' . $queryStr;
                            }
                        }
                    }
                }
            }
            
       // }

        return false;
}
function practicon_get_next_page($queryStr){
    $res = explode('&page=' , $queryStr);
    if(count($res) > 1){
        $page = (int) $res[1];
        return $res[0] . '&page=' . ($page + 1);
    }
    return false;
}

