<?php
function parse_category($html, $task){
    $out = array();
    $out['products'] = parse_products($html , $task);
    $out['next_page'] = parse_next_page($html , $task);
    return $out;
}        
        
function parse_products($html, $task){
    
    $out = array();
    $instruction = 'a.product-loop-title';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    unset($parser);
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $pos_product){
            if(isset($pos_product['href']) && !is_array($pos_product['href']) && strlen($pos_product['href']) > 0){
                $out[] = $pos_product['href'];
            }
        }
        $out = array_unique($out);
        return $out;
    }

}

function parse_next_page($html , $task){

    $instruction = "a.page-numbers";
    $parser = new nokogiri($html);
    $count = $parser->get($instruction)->toArray();
    unset($parser);
    if(isset($count[0]) && is_array($count[0]) && count($count[0]) > 0){
        foreach($count as $con){
            $can = $con['href'];
            
            $queryStr_data = orthotechnology_get_next_page($con['href']);
                if($queryStr_data){
                    return $queryStr_data;
                }	
            }
        }
return false;
}
function orthotechnology_get_next_page($queryStr){
    if($queryStr != ""){
	return $queryStr;
    }
return false;
}
?>