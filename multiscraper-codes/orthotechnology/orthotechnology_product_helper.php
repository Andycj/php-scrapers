<?php
function mspro_orthotechnology_title($html){
    $title = '';
    $instruction = 'h2.product_title';
    $parser = new nokogiri($html);
    $data = $parser->get($instruction)->toArray();
    unset($parser);
    if (isset($data[0]['#text'])) {
        $titl =  $data[0]['#text'];
        $title = preg_replace('|[^A-Za-z09\-,;.& ]|', '', $titl);
        return $title;
    }
    return '';
}

function mspro_orthotechnology_manufacture($html){
    return 'Ortho Technology';
}



function mspro_orthotechnology_description($html){
    $resa = '';
    $pq = phpQuery::newDocumentHTML($html);
    $text  = $pq->find('#tab-description');
    $resa .=$text->html();
    $pat = array('|<a href="(.*)">|', '|<img (.*)">|', '|Download|');
    $resb = preg_replace($pat, '', $resa);
    return $resb;
}


function mspro_orthotechnology_price($html){
    $price = '';
    $instruction = 'p.price span.woocommerce-Price-amount';
    $parser = new nokogiri($html);
    $date = $parser->get($instruction)->toArray();
    unset($parser);
    if (isset($date[0]['#text'])) {
        foreach ($date[0]['#text'] as $price){
            $prices =  preg_replace('|,|', '', $price);
        }
        return $prices;
    }
    return '';	
}


function mspro_orthotechnology_special_price($html){
    return '';	
}

function mspro_orthotechnology_sku($html){
    return mspro_orthotechnology_model($html);
}

function mspro_orthotechnology_model($html){
    return '';
}

function mspro_orthotechnology_meta_description($html){
    $meta = '';
    $res =  explode('<meta name="twitter:description" content="', $html);
    if(count($res) > 1){
        $res1 = explode('"' , $res[1]);
        if(count($res1) > 1){
            $metas = $res1[0];
            return $metas;
        }
    }
    return '';
}

function mspro_orthotechnology_meta_keywords($html){
    return mspro_orthotechnology_title($html);
}


function mspro_orthotechnology_main_image($html){
    $arr = '';
    $arr = mspro_orthotechnology_get_images_arr($html);
    if(isset($arr[0]) && strlen($arr[0]) > 0){
        return $arr[0];
    }
return '';
}


function mspro_orthotechnology_other_images($html){
    $arr = '';
    $arr = mspro_orthotechnology_get_images_arr($html);
    if(count($arr) > 1){
        unset($arr[0]);
        return $arr;
    }
return '';
}

Function mspro_orthotechnology_get_images_arr($html){
    $out = '';
    $out = array();
    $instruction = 'div.img-thumbnail div.inner img';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach ($res as $img) {
            $outs = $img['src'];
            if (strpos($outs, 'uploads') !== false){
                $out[] = $outs;
            }
            $out = array_unique($out);
        }
        return $out;
    }
return '';   
}


function mspro_orthotechnology_options($html){
    $out = array();
    $opt_name = '#nipv-tablesorter tr td';
    $parser = new nokogiri($html);
    $res = $parser->get($opt_name)->toArray();
    if (isset($res) && is_array($res) && count($res)>0){
        foreach($res as $sopt_name){
            if (!is_array($sopt_name['#text'])){
                 if(strpos($sopt_name['#text'], " - ") == True){
                    $value[] = trim($sopt_name['#text']);
                    
                    $OPTION = array();
                    $OPTION['name'] = "Select option";
                    $OPTION['type'] = "select";
                    $OPTION['required'] = true;
                    $OPTION['values'] = array();
                    foreach($value as $option_value){
                        if(isset($option_value) && !is_array($option_value) && strlen(trim($option_value)) > 0 ){
                            $OPTION['values'][] = array('name' => trim($option_value) , 'price' => 0);
                        }
                    }
                }
            }
        }
        if(count($OPTION['values']) > 0){
            $out[] = $OPTION;
        }
        return $out;
    }
return '';
}

function mspro_orthotechnology_noMoreAvailable($html){
    return true;
}