<?php


function parse_category($html  , $task){
		$out = array();
		$out['products'] = parse_products($html , $task);
		$out['next_page'] = parse_next_page($html , $task);
		return $out;
}


function parse_products($html , $task){
		$result = array();
		
		$link = 'a.pro-thumb';
        $parser = new nokogiri($html);
        $links = $parser->get($link)->toArray();
        //echo '<pre>'.print_r($links , 1).'</pre>';exit;
        unset($parser);
        if (count($links) > 0) {
            foreach ($links as $value) {
            	if(isset($value['href'])){
                	$result[] = $value['href'];
            	}
            }
        }
        
        if(count($result) < 1){
            $link = 'a.product-link';
            $parser = new nokogiri($html);
            $links = $parser->get($link)->toArray();
            //echo '<pre>'.print_r($links , 1).'</pre>';exit;
            unset($parser);
            if (count($links) > 0) {
                foreach ($links as $value) {
                    if(isset($value['href'])){
                        $result[] = $value['href'];
                    }
                }
            }
        }
        
        if(count($result) > 0){
            foreach($result as $key => $value){
                if(substr($value, 0 , 2) == "//"){
                    $result[$key] = 'https:' . $value;
                }
            }
        }
        
        $result = array_unique($result);
		//echo '<pre>'.print_r($result , 1).'</pre>';
        return $result;
}


function parse_next_page($html , $task){
        $nextPage = 'a.next';
        $parser = new nokogiri($html);
        $next = $parser->get($nextPage)->toArray();
        unset($parser);
        //$next = reset($next);
		//echo '<pre>'.print_r($next , 1).'</pre>';exit;
        if (isset($next[0]['href']) && isset($next[0]['title']) && trim($next[0]['title']) == 'Next Page'){
        	return 'https://www.overstock.com' . trim($next[0]['href']);
        }

        return false;
}