<?php
function mspro_practicon_title($html){
    $instruction = 'h1';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	 	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text']));
    }
 	if (isset($data[0]['#text'][0]) && !is_array($data[0]['#text'][0])) {
	   	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text'][0]));
    }   
    return '';
}

function mspro_practicon_description($html){
	$res = '';
		
	$pq = phpQuery::newDocumentHTML($html);

	$temp  = $pq->find('div.longdescription-section');
	foreach ($temp as $block){
		$res .=  $temp->html() . '<br />';
	}
	
	/*$res = preg_replace('/<img(?:\\s[^<>]*)?>/i', '', $res);*/
	return $res;
}


function mspro_practicon_price($html){
    
	$instruction = 'span.pricing';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	    $price = preg_replace("/[^0-9.]/", "",  trim($data[0]['#text']) );
	    return (float) $price;
	}
	return '';
	
}

function mspro_practicon_special_price($html){
    
	$instruction = 'span.pricing';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	    $special_price = preg_replace("/[^0-9.]/", "",  trim($data[1]['#text']) );
		return (float) $special_price;
		
	}
	return '';
	
}


function mspro_practicon_sku($html){
    return mspro_practicon_model($html);
}

function mspro_practicon_model($html){
    $instruction = 'div.pds-secondary-item-info #itemNumberLabel';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
	if($res){
		return trim($res[0]['data-itemno']);
	}
	return '';
}


function mspro_practicon_meta_description($html){
        $res =  explode('<meta name="description" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "practicon") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
       return '';
}

function mspro_practicon_meta_keywords($html){
        $res =  explode('<meta name="keywords" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "practicon") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
       return mspro_practicon_title($html);
}


function mspro_practicon_main_image($html){
	$arr = mspro_practicon_get_images_arr($html);
	if(isset($arr[0]) && strlen($arr[0]) > 0){
		return $arr[0];
	}
	return '';
}


function mspro_practicon_other_images($html){
	$arr = mspro_practicon_get_images_arr($html);
	if(count($arr) > 1){
		unset($arr[0]);
		return $arr;
	}
	return array();
}


function mspro_practicon_get_images_arr($html){
        $out = array();
        $instruction = 'img#ProductImage';
        $parser = new nokogiri($html);
		$res = $parser->get($instruction)->toArray();
        //echo '<pre>'.print_r($res , 1).'</pre>';exit;
        if(isset($res[0]['src']) && !is_array($res[0]['src']) && strlen($res[0]['src']) > 0){
            $out[] = $res[0]['src'];
		} 
        $url =  explode("/",$out[0]);
				
        $instruction1 = 'div#gallery ul li';
        $parser1 = new nokogiri($html);
        $res1 = $parser1->get($instruction1)->toArray();
		
        //echo '<pre>'.print_r($res , 1).'</pre>';exit;
		if(isset($res1) && is_array($res1) && count($res1) > 0){
			foreach($res1 as $pos_image){
				if(isset($pos_image['a'][0]['href']) && !is_array($pos_image['a'][0]['href']) && strlen($pos_image['a'][0]['href']) > 0 ){
					array_shift($pos_image['a'][0]['href']);
					$out[] = '//'.$url[2].str_replace("//","",$pos_image['a'][0]['href']);
				}
            }
        }
        
        foreach($out as $k => $v){
            $out[$k] = str_replace(array(" ") , array("%20"), $v);
        }
        
        $out = array_unique($out);
		return $out;
}




function mspro_practicon_options($html){
    $out = array();

   /*  $instruction = 'div.item-selection-wrapper select';
    $out = array();
     */
    $options_NAMES = array();
    $instruction1 = 'div.item-selection-wrapper select';
    $parser1 = new nokogiri($html);
    $res1 = $parser1->get($instruction1)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
	if(isset($res1) && is_array($res1) && count($res1) > 0){
        foreach($res1 as $pos_option_name){
            if(isset($pos_option_name['option']) && count($pos_option_name['option']) > 0){
				$options_NAMES[] = str_replace("-"," ",$pos_option_name['option'][0]['#text']);
            }
        }
    }
	
    $instruction = 'div.item-selection-wrapper select';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
    unset($parser);
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $key => $POS_option){
            if(isset($POS_option['option']) && is_array($POS_option['option']) && count($POS_option['option']) > 0){
				array_shift($POS_option['option']);

                $OPTION = array();
                $OPTION['name'] = trim(str_replace( array(":") , array("") , $options_NAMES[$key]));
                $OPTION['type'] = "select";
                $OPTION['required'] = true;
                $OPTION['values'] = array();
                foreach($POS_option['option'] as $option_value){
                    if(isset($option_value['#text']) && !is_array($option_value['#text']) && strlen(trim($option_value['#text'])) > 0 ){
                        $OPTION['values'][] = array('name' => $option_value['#text'] , 'price' => 0);
                    }
                }
                if(count($OPTION['values']) > 0){
                    $out[] = $OPTION;
                }
            }
        }
    }
	/* $myfile = fopen("/home/demodenta/public_html/newfile.txt", "w") or die("Unable to open file!");
	fwrite($myfile, $out);
	fclose($myfile); */
    //echo '<pre>'.print_r($out , 1).'</pre>';exit;
    
    return $out;
}


function mspro_practicon_noMoreAvailable($html){
    if(!stripos($html , '<span>In stock</span>') > 0){
        return true;
    }
	return false;
}
