<?php


function mspro_duralinesystems_title($html){
    $instruction = 'h1';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	 	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text']));
    }
    if (isset($data[1]['#text']) && !is_array($data[1]['#text'])) {
        return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[1]['#text']));
    }
 	if (isset($data[0]['#text'][0]) && !is_array($data[0]['#text'][0])) {
	   	return trim(str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" ) ,$data[0]['#text'][0]));
    }   
    return '';
}

function mspro_duralinesystems_description($html){
	$res = '';
		
	$pq = phpQuery::newDocumentHTML($html);
	
	$temp  = $pq->find('section#prod-more');
	foreach ($temp as $block){
	   $res .= $temp->html();
	}
	
	$temp_spec  = $pq->find('section#prod-attributes');
	foreach ($temp_spec as $block){
	   //$res .= $temp_spec->html();
	   $res .= str_replace('<span class="faux-h2">Specs:</span>','<h3><strong>Specifications</strong></h3>',$temp_spec->html());
	}
	return $res;
}


function mspro_duralinesystems_price($html){
    $instruction = 'span[class=pi-cur-price]';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	//echo '<pre>'.print_r($data , 1).'</pre>';exit;
	if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
	    $price = preg_replace("/[^0-9.]/", "",  trim($data[0]['#text']) );
	    return (float) $price;
	}
	if (isset($data[0]['#text'][1]) && !is_array($data[0]['#text'][1])) {
	    $price = preg_replace("/[^0-9.]/", "",  trim($data[0]['#text'][1]) );
		return (float) $price;
	}
    return '';
}


function mspro_duralinesystems_sku($html){
    return mspro_duralinesystems_model($html);
}

function mspro_duralinesystems_model($html){
    $res = explode('<span itemprop="sku">' , $html);
	
    if(count($res) > 1){
        $res = explode('<' , $res[1] , 2);
        if(count($res) > 1){
            return trim($res[0]);
        }
    }
    return '';
}


function mspro_duralinesystems_meta_description($html){
        $res =  explode('<meta name="description" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "duralinesystems") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
       return '';
}

function mspro_duralinesystems_meta_keywords($html){
        $res =  explode('<meta name="keywords" content="' , $html);
       if(count($res) > 1){
       		$res = explode('"' , $res[1]);
       		if(count($res) > 1){
       			return str_replace(array("&nbsp;" , "&amp;" , "duralinesystems") , array(" " , "`" , "") , $res[0]);	
       		}
       		 
       }
       return mspro_duralinesystems_title($html);
}


function mspro_duralinesystems_main_image($html){
	$arr = mspro_duralinesystems_get_images_arr($html);
	if(isset($arr[0]) && strlen($arr[0]) > 0){
		return $arr[0];
	}
	return '';
}


function mspro_duralinesystems_other_images($html){
	$arr = mspro_duralinesystems_get_images_arr($html);
	if(count($arr) > 1){
		unset($arr[0]);
		return $arr;
	}
	return array();
}


function mspro_duralinesystems_get_images_arr($html){
     $out = array();
        $instruction = 'section #product-image img';
        $parser = new nokogiri($html);
		$res = $parser->get($instruction)->toArray();
		
        //echo '<pre>'.print_r($res , 1).'</pre>';exit;
        if(isset($res[0]['src']) && !is_array($res[0]['src'])){
            $out[] = '//www.duralinesystems.com'.$res[0]['src'];
		} 
		//$url =  explode("/",$out[0]);
				
        $instruction1 = 'section#product-images ul li';
        $parser1 = new nokogiri($html);
        $res1 = $parser1->get($instruction1)->toArray();
		
        //echo '<pre>'.print_r($res , 1).'</pre>';exit;
		if(isset($res1) && is_array($res1) && count($res1) > 0){
			foreach($res1 as $pos_image){
				if(isset($pos_image['a'][0]['href']) && !is_array($pos_image['a'][0]['href'])){
					array_shift($pos_image['a'][0]['href']);
					//$out[] = '//'.$url[2].str_replace("//","",$pos_image['a'][0]['href']);
					$out[] = '//www.duralinesystems.com'.$pos_image['a'][0]['href'];
				}
            }
        }
        foreach($out as $k => $v){
            $out[$k] = str_replace(array(" ") , array("%20"), $v);
        }
        
        $out = array_unique($out);
		return $out;  
}




function mspro_duralinesystems_options($html){
    $out = array();
    
    $options_NAMES = array();
    $instruction = 'div.four label';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
	//echo '<pre>'.print_r($res , 1).'</pre>';exit;
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $pos_option_name){
            if(isset($pos_option_name['#text']) && !is_array($pos_option_name['#text']) ){
                $options_NAMES[] = $pos_option_name['#text'];
            }
        }
    }

    $instruction = 'div.four select';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
    unset($parser);
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $key => $POS_option){
			
            if(isset($POS_option['option']) && is_array($POS_option['option']) && count($POS_option['option']) > 0 && isset($options_NAMES[$key]) ){
				array_shift($POS_option['option']);
				
                $OPTION = array();
                $OPTION['name'] = trim(str_replace( array(":") , array("") , $options_NAMES[$key]));
                $OPTION['type'] = "select";
                $OPTION['required'] = true;
                $OPTION['values'] = array();
                foreach($POS_option['option'] as $option_value){
                    if(isset($option_value['#text']) && !is_array($option_value['#text']) && strlen(trim($option_value['#text'])) > 0 ){
                        $OPTION['values'][] = array('name' => $option_value['#text'] , 'price' => 0);
                    }
                }
                if(count($OPTION['values']) > 0){
                    $out[] = $OPTION;
                }
            }
        }
    }
	/* $myfile = fopen("/home/demodenta/public_html/testtext/newfile.txt", "w") or die("Unable to open file!");
	fwrite($myfile, print_r($out,true));
	fclose($myfile); */
    //echo '<pre>'.print_r($out , 1).'</pre>';exit;
    
    return $out;
}


function mspro_duralinesystems_noMoreAvailable($html){
    if(!stripos($html , '<span>In stock</span>') > 0){
        return true;
    }
	return false;
}
