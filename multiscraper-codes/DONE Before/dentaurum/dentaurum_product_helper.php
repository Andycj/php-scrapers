<?php
function mspro_dentaurum_title($html){
    $instruction = 'div.product-detail-section h3';
    $parser = new nokogiri($html);
    $data = $parser->get($instruction)->toArray();
    unset($parser);
    if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
        return $data[0]['#text'].'<br>';
    }
	return '';
}

function mspro_dentaurum_manufacture($html){
    return 'Dentaurum Inc.';
}

function mspro_dentaurum_description($html){
    $res = '';
    $resa = '';
    $inter = '<p><h3>Product Information</h3></p>';
    $pq = phpQuery::newDocumentHTML($html);
    $text  = $pq->find('#description div.row div.text');

    $res .=$text->html();
    $table = $pq->find('#description div.row div.clearfix');
    $resa .=$table->html();
    $descr = $res . $inter . $resa;
    return $descr;
}

function mspro_dentaurum_price($html){
	return '';	
}

function mspro_dentaurum_special_price($html){
	return '';	
}

function mspro_dentaurum_sku($html){
    return mspro_dentaurum_model($html);
}

function mspro_dentaurum_model($html){
    $instruction = '#submit_merken';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    unset ($parser);
    if($res){
        $data = $res[0]['onclick'];
        $ares = explode("=", $data);
        $area = explode('&', $ares[2]);
        $res = $area[0];
        return $res;
    }
	return '';
}

function mspro_dentaurum_meta_description($html){
    $res =  explode('<meta name="description" content="' , $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
        return $res[0];	
        }
    }
	return '';
}

function mspro_dentaurum_meta_keywords($html){
    $res =  explode('<meta name="description" content="' , $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
            $resa = str_replace(array("&nbsp;" , "&amp;" , "-", " ") , array("," , "," , ",", ",") , $res[0]);
            $res = str_replace(array(",,", ",,,", ",,,,"), ",", $resa);
            return $res;	
        }
    }
	return mspro_dentaurum_title($html);
}

function mspro_dentaurum_main_image($html){
    $arr = mspro_dentaurum_get_images_arr($html);
    if(isset($arr[0]) && strlen($arr[0]) > 0){
        return $arr[0];
    }
	return '';
}


function mspro_dentaurum_other_images($html){
    $arr = mspro_dentaurum_get_images_arr($html);
    if(count($arr) > 1){
        unset($arr[0]);
        return $arr;
    }
	return array();
}

function mspro_dentaurum_get_images_arr($html){
    $out = array();
    $instruction = 'a.cloud-zoom img';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach ($res as $img) {
            $out[] = 'https://shop.dentaurum.us/'.$img['src'];
        }
    $out = array_unique($out);
    return $out; 
    } 
}

function mspro_dentaurum_options($html){
	return '';
}

function mspro_dentaurum_noMoreAvailable($html){
    return true;
}
