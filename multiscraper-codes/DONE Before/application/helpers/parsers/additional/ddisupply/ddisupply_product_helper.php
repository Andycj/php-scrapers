<?php
function mspro_ddisuppli_title($html){
    $instruction = 'h1'; // gather title and clean up the text
    $parser = new nokogiri($html);
    $data = $parser->get($instruction)->toArray();
    unset($parser);
    //echo '<pre>'.print_r($data , 1).'</pre>';exit;
    if(isset($data) && is_array($data) && count($data) > 0){
        foreach($data as $title){
            $title1 = $title['#text'];
        }
    }
    //echo '<pre>'.print_r($title1).'</pre>';exit;
    return $title1;
}

function mspro_ddisupply_description($html){
	$res = '';
	$pq = phpQuery::newDocumentHTML($html);
	$temp  = $pq->find('#ProductDescription div'); // gather description
        //echo '<pre>'.print_r($temp).'</pre>';exit;
	foreach ($temp as $block){
	   $res .= $temp->html();
	}
        //echo '<pre>'.print_r($res).'</pre>';exit;
	return $res;
}


function mspro_ddisupply_price($html){
    $instruction = 'span.ProductPrice';
    $parser = new nokogiri($html);
    $data = $parser->get($instruction)->toArray();
    unset($parser);
    //echo '<pre>'.print_r($data , 1).'</pre>';exit;
    $data1 = $data[1]['#text'];
    $price = preg_replace("/[^0-9.]/", "",  trim($data1) );
    //echo '<pre>'.print_r($price).'</pre>';exit;

    return $price;
}


function mspro_ddisupply_sku($html){
    return mspro_ddisupply_model($html);
}

function mspro_ddisupply_model($html){
	$instruction = 'span.VariationProductSKU';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	$sku = $data[0]['#text'];
        //echo '<pre>'.print_r($sku).'</pre>';exit;
    return '';
}


function mspro_ddisupply_meta_description($html){
    $res =  explode('<meta name="description" content="' , $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
            $res1 = str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" , "") , $res[0]);
            //echo '<pre>'.print_r($res1).'</pre>';exit;
            //return str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" , "") , $res[0]);	
        }
    }
    return $res1;
}

function mspro_ddisupply_keywords($html){
    $res =  explode('<meta property="og:description" content="' , $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
            $res1 =  str_replace(array("&nbsp;" , "&amp;", ) , array(" " , "`" , "") , $res[0]);
            $res2 = str_replace(", " , ",", $res1);
            //return str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" , "") , $res[0]);
            //echo '<pre>'.print_r($res2).'</pre>';exit;
        }
    }
    return $res2;
}


function mspro_ddisupply_main_image($html){
	$arr = mspro_ddisupply_get_images_arr($html);
	if(isset($arr[0]) && strlen($arr[0]) > 0){
		return $arr[0];
	}
	return '';
}


function mspro_ddisupply_other_images($html){
	$arr = mspro_ddisupply_get_images_arr($html);
	if(count($arr) > 1){
		unset($arr[0]);
		return $arr;
	}
	return array();
}


function mspro_ddisupply_get_images_arr($html){
    $out = array();
    $instruction1 = 'div.TinyOuterDiv div a';
    $parser1 = new nokogiri($html);
    $res1 = $parser1->get($instruction1)->toArray();
    unset($parser1);
    foreach($res1 as $aimg){
        $res2 = $aimg['img']['src'];
        $res2 = str_replace('60.90', '500.750', $res2);
        $out[] = $res2;
        $out = array_unique($out);
    }
    //echo '<pre>'.print_r($out).'</pre>';
    $out = array_unique($out);
    return $out;  
}


/*
function mspro_ddisupply_options($html){
    $out = array();
    
    $options_NAMES = array();
    $instruction = 'div.productAttributeLabel label span.name'; // gather options name's
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
	//echo '<pre>'.print_r($res , 1).'</pre>';exit;
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $pos_option_name){
            if(isset($pos_option_name['#text']) && !is_array($pos_option_name['#text']) ){
                $options_NAMES[] = $pos_option_name['#text'];
            }
        }
    }

    $instruction = 'div.productAttributeValue div div select'; //gather options name values
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
    unset($parser);
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $key => $POS_option){
			
            if(isset($POS_option['option']) && is_array($POS_option['option']) && count($POS_option['option']) > 0 && isset($options_NAMES[$key]) ){
				array_shift($POS_option['option']);
				
                $OPTION = array();
                $OPTION['name'] = trim(str_replace( array(":") , array("") , $options_NAMES[$key]));
                $OPTION['type'] = "select";
                $OPTION['required'] = true;
                $OPTION['values'] = array();
                foreach($POS_option['option'] as $option_value){
                    if(isset($option_value['#text']) && !is_array($option_value['#text']) && strlen(trim($option_value['#text'])) > 0 ){
                        $OPTION['values'][] = array('name' => $option_value['#text'] , 'price' => 0);
                    }
                }
                if(count($OPTION['values']) > 0){
                    $out[] = $OPTION;
                }
            }
        }
    }
$myfile = fopen("/home/demodenta/public_html/testtext/newfile.txt", "w") or die("Unable to open file!");
	fwrite($myfile, print_r($out,true));
	fclose($myfile);
    //echo '<pre>'.print_r($out , 1).'</pre>';exit;
    
    return '';
}*/


function mspro_ddisupply_noMoreAvailable($html){
    return true;
}
