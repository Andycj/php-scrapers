<?php
function mspro_ddisupply_title($html){
	$instruction = 'h1'; // gather title and clean up the text
    $parser = new nokogiri($html);
    $data = $parser->get($instruction)->toArray();
    unset($parser);
    if(isset($data) && is_array($data) && count($data) > 0){
        foreach($data as $title){
            return $title['#text'];
        }
    }
    return '';
}

function mspro_ddisupply_manufacture($html){
    $instruction = 'h4.BrandName a span';
    $parser = new nokogiri($html);
    $data = $parser->get($instruction)->toArray();
    unset($parser);
    if (isset($data) && is_array($data)) {
        foreach ($data as $man){
            $manuf = $man['#text'];
            //echo '<pre>'.$manuf.'<pre>';
            return trim($manuf);
        }
    } else {
        return 'NO Data';
    }
}

function mspro_ddisupply_description($html){
	$res = '';
	$pq = phpQuery::newDocumentHTML($html);
	$temp  = $pq->find('#ProductDescription div'); // gather description
	foreach ($temp as $block){
	   $res .= $temp->html();
	}
	return $res;
}

function mspro_ddisupply_price($html){
    $res =  explode('<meta itemprop="price" content="' , $html);
    if(count($res) > 1){
    	$res = explode('"' , $res[1]);
    	if(count($res) > 1){
    		$res1 = str_replace(array("&nbsp;" , "&amp;" , "practicon") , array(" " , "`" , "") , $res[0]);	
                return $res1;
       	}
       		 
    }
    return "";
}

function mspro_ddisupply_sku($html){
    return mspro_ddisupply_model($html);
}

function mspro_ddisupply_model($html){
	$instruction = 'span.VariationProductSKU';
	$parser = new nokogiri($html);
	$data = $parser->get($instruction)->toArray();
	unset($parser);
	if ($data){
		return trim($data[0]['#text']);
	}
    return '';
}

function mspro_ddisupply_meta_description($html){
	$res =  explode('<meta name="description" content="' , $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
            return str_replace(array("&nbsp;" , "&amp;") , array(" " , "`" , "") , $res[0]);
        }
    }
    return '';
}

function mspro_ddisupply_meta_keywords($html){
	$res =  explode('<meta property="og:description" content="' , $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
            $res1 =  str_replace(array("&nbsp;" , "&amp;", ) , array(" " , "`" , "") , $res[0]);
            return str_replace(", " , ",", $res1);
        }
    }
    return mspro_ddisupply_title($html);
}

function mspro_ddisupply_main_image($html){
	$arr = mspro_ddisupply_get_images_arr($html);
	if(isset($arr[0]) && strlen($arr[0]) > 0){
		return $arr[0];
	}
	return '';
}


function mspro_ddisupply_other_images($html){
	$arr = mspro_ddisupply_get_images_arr($html);
	if(count($arr) > 1){
		unset($arr[0]);
		return $arr;
	}
	return array();
}


function mspro_ddisupply_get_images_arr($html){
    $out = array();
    $instruction1 = 'div.TinyOuterDiv div a';
    $parser1 = new nokogiri($html);
    $res1 = $parser1->get($instruction1)->toArray();
    unset($parser1);
    if(isset($res1) && is_array($res1) && count($res1) > 0){
        foreach($res1 as $aimg){
            $res2 = $aimg['img']['src'];
            $res2 = str_replace('60.90', '500.750', $res2);
            $out[] = $res2;

	}
    }
    $out = array_unique($out);
    //echo '<pre>'.print_r($out).'</pre>';
    return $out;
}



function mspro_ddisupply_options($html){
    $out = array();
    $options_NAMES = array();

    $instruction1 = 'div.productAttributeLabel label span.name';
    $parser1 = new nokogiri($html);
    $res1 = $parser1->get($instruction1)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
    if(isset($res1) && is_array($res1) && count($res1) > 0){
        foreach($res1 as $pos_option_name){
            if(isset($pos_option_name['#text']) && count($pos_option_name['#text']) > 0){
		$options_NAMES[] = trim(str_replace(":","",$pos_option_name['#text']));
                //echo '<pre>'.print_r($options_NAMES).'</pre>';
            }
        }
    }
	
    $instruction = 'select.validation';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
    unset($parser);
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $key => $POS_option){
            if(isset($POS_option['option']) && is_array($POS_option['option']) && count($POS_option['option']) > 0){
				array_shift($POS_option['option']);

                $OPTION = array();
                $OPTION['name'] = trim(str_replace( array(":") , array("") , $options_NAMES[$key]));
                $OPTION['type'] = "select";
                $OPTION['required'] = true;
                $OPTION['values'] = array();
                foreach($POS_option['option'] as $option_value){
                    if(isset($option_value['#text']) && !is_array($option_value['#text']) && strlen(trim($option_value['#text'])) > 0 ){
                        $OPTION['values'][] = array('name' => $option_value['#text'] , 'price' => 0);
                        //echo '<pre>'.print_r($OPTION['values']).'</pre>';
                    }
                }
                if(count($OPTION['values']) > 0){
                    $out[] = $OPTION;
                    //echo '<pre>'.print_r($out).'</pre>';
                }
            }
        }
    }
    return $out;
}


function mspro_ddisupply_noMoreAvailable($html){
        return true;
}
