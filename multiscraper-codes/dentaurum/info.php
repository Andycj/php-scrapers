<?php
$config['additionalmarkets'][] = array("name" => "dentaurum" ,
							 "title" => "<b>Dentaurum.us</b>",
							 "url_aliases" => array("dentaurum.com" , "dentaurum", "shop.dentaurum.us", "dentaurum.us"),
							 "fields" => array("title"  , "description"));

?>