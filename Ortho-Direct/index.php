<!DOCTYPE html>
<!--
This will be the scraper for Ortho-Direct website
Scraper wil crawl each and every category on site and return data
as csv file
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>O-D Scraper</title>
    </head>
    <body>
        <?php
        require '/start_data.csv';
        require 'libs/simple_html_dom/simple_html_dom.php';
        require 'libs/Classes/PHPExcel.php'; //excel composer
        
        const lang = "1";
        const supplier_id = "51";
        //for now cat_id are constants!
        const cat_id = "949";
        const qty = '10000';
        const req_shipp = 'yes';
        const points = '0';
        const weight = '1,00';
        const unit = 'lb';
        const length = '1';
        const width = '1';
        const height = '1';
        const length_unit = 'in';
        const status_enabled = 'true';
        const tax_class_id = '9';
        const store_ids = '0,1,4';
        const substract = 'true';
        const sort_order = '0';
        const stock_status_id = '7';

        $date_added = date("Y-m-d H-i-s");
        
        //Setting Excel file / sheets and headers of sheets
{        
        $phpEx = new PHPExcel();
        
        //Categories
        $objWorkSheet = $phpEx->createSheet(0);    
        $phpEx->setActiveSheetIndex(0);//second sheet is Categories!
        $phpEx->getActiveSheet()->setTitle('Categories');
        
        //Set header for categories sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "category_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "parent_id");
        $phpEx->getActiveSheet()->setCellValue("C1", "name");
        $phpEx->getActiveSheet()->setCellValue("D1", "top");
        $phpEx->getActiveSheet()->setCellValue("E1", "columns");
        $phpEx->getActiveSheet()->setCellValue("F1", "sort_order");
        $phpEx->getActiveSheet()->setCellValue("G1", "image_name");
        $phpEx->getActiveSheet()->setCellValue("H1", "date_added");
        $phpEx->getActiveSheet()->setCellValue("I1", "date_modified");
        $phpEx->getActiveSheet()->setCellValue("J1", "language_id");
        $phpEx->getActiveSheet()->setCellValue("K1", "seo_keyword");
        $phpEx->getActiveSheet()->setCellValue("L1", "description");
        $phpEx->getActiveSheet()->setCellValue("M1", "meta_description");
        $phpEx->getActiveSheet()->setCellValue("N1", "meta_keywords");
        $phpEx->getActiveSheet()->setCellValue("O1", "store_ids");
        $phpEx->getActiveSheet()->setCellValue("P1", "layout");
        $phpEx->getActiveSheet()->setCellValue("Q1", "status enabled");
        
        //Products
        $objWorkSheet = $phpEx->createSheet(1);    
        $phpEx->setActiveSheetIndex(1);//second sheet is Products!
        $phpEx->getActiveSheet()->setTitle('Products');
            
        //Set header for Products sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "name");
        $phpEx->getActiveSheet()->setCellValue("C1", "categories");
        $phpEx->getActiveSheet()->setCellValue("D1", "sku");
        $phpEx->getActiveSheet()->setCellValue("E1", "upc");
        $phpEx->getActiveSheet()->setCellValue("F1", "ean");
        $phpEx->getActiveSheet()->setCellValue("G1", "jan");
        $phpEx->getActiveSheet()->setCellValue("H1", "isbn");
        $phpEx->getActiveSheet()->setCellValue("I1", "mpn");
        $phpEx->getActiveSheet()->setCellValue("J1", "location");
        $phpEx->getActiveSheet()->setCellValue("K1", "quantity");
        $phpEx->getActiveSheet()->setCellValue("L1", "model");
        $phpEx->getActiveSheet()->setCellValue("M1", "manufacturer");
        $phpEx->getActiveSheet()->setCellValue("N1", "image_name");
        $phpEx->getActiveSheet()->setCellValue("O1", "requires shipping");
        $phpEx->getActiveSheet()->setCellValue("P1", "price");
        $phpEx->getActiveSheet()->setCellValue("Q1", "points");
        $phpEx->getActiveSheet()->setCellValue("R1", "date_added");
        $phpEx->getActiveSheet()->setCellValue("S1", "date_modified");
        $phpEx->getActiveSheet()->setCellValue("T1", "date_available");
        $phpEx->getActiveSheet()->setCellValue("U1", "weight");
        $phpEx->getActiveSheet()->setCellValue("V1", "unit");
        $phpEx->getActiveSheet()->setCellValue("W1", "lenght");
        $phpEx->getActiveSheet()->setCellValue("X1", "width");
        $phpEx->getActiveSheet()->setCellValue("Y1", "height");
        $phpEx->getActiveSheet()->setCellValue("Z1", "lenght unit");
        $phpEx->getActiveSheet()->setCellValue("AA1", "status enabled");
        $phpEx->getActiveSheet()->setCellValue("AB1", "tax_class_id");
        $phpEx->getActiveSheet()->setCellValue("AC1", "viewed");
        $phpEx->getActiveSheet()->setCellValue("AD1", "language_id");
        $phpEx->getActiveSheet()->setCellValue("AE1", "seo_keyword");
        $phpEx->getActiveSheet()->setCellValue("AF1", "description");
        $phpEx->getActiveSheet()->setCellValue("AG1", "meta_description");
        $phpEx->getActiveSheet()->setCellValue("AH1", "meta_keywords");
        $phpEx->getActiveSheet()->setCellValue("AI1", "status_id");
        $phpEx->getActiveSheet()->setCellValue("AJ1", "store_ids");
        $phpEx->getActiveSheet()->setCellValue("AK1", "layout");
        $phpEx->getActiveSheet()->setCellValue("AL1", "related_ids");
        $phpEx->getActiveSheet()->setCellValue("AM1", "tags");
        $phpEx->getActiveSheet()->setCellValue("AN1", "sort_order");
        $phpEx->getActiveSheet()->setCellValue("AO1", "subtract");
        $phpEx->getActiveSheet()->setCellValue("AP1", "minimum");
        $phpEx->getActiveSheet()->setCellValue("AQ1", "supplier_id");
        $phpEx->getActiveSheet()->setCellValue("AR1", "cost");
        $phpEx->getActiveSheet()->setCellValue("AS1", "gross_profit_percent");
        $phpEx->getActiveSheet()->setCellValue("AT1", "alternate_id"); //end of header!
        
        //AdditionalImages
        $objWorkSheet = $phpEx->createSheet(2);
        $phpEx->setActiveSheetIndex(2);//third sheet is AdditionalImages!
        $phpEx->getActiveSheet()->setTitle('AdditionalImages');
        
        //Set header for AdditionalImages sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "image");
        $phpEx->getActiveSheet()->setCellValue("C1", "sort_order");
        
        //Options
        $objWorkSheet = $phpEx->createSheet(3);
        $phpEx->setActiveSheetIndex(3);//4-th sheet is Options!
        $phpEx->getActiveSheet()->setTitle('Options');
                
        //Set header for Options sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "language_id");
        $phpEx->getActiveSheet()->setCellValue("C1", "option");
        $phpEx->getActiveSheet()->setCellValue("D1", "type");
        $phpEx->getActiveSheet()->setCellValue("E1", "value");
        $phpEx->getActiveSheet()->setCellValue("F1", "image");
        $phpEx->getActiveSheet()->setCellValue("G1", "required");
        $phpEx->getActiveSheet()->setCellValue("H1", "quantity");
        $phpEx->getActiveSheet()->setCellValue("I1", "substract");
        $phpEx->getActiveSheet()->setCellValue("J1", "price");
        $phpEx->getActiveSheet()->setCellValue("K1", "price prefix");
        $phpEx->getActiveSheet()->setCellValue("L1", "points");
        $phpEx->getActiveSheet()->setCellValue("M1", "points prefix");
        $phpEx->getActiveSheet()->setCellValue("N1", "weight");
        $phpEx->getActiveSheet()->setCellValue("O1", "weight prefix");
        $phpEx->getActiveSheet()->setCellValue("P1", "sort_order");
        $phpEx->getActiveSheet()->setCellValue("Q1", "cost");
        $phpEx->getActiveSheet()->setCellValue("R1", "cost_prefix");
        $phpEx->getActiveSheet()->setCellValue("S1", "sku");
        $phpEx->getActiveSheet()->setCellValue("T1", "mpn");
        $phpEx->getActiveSheet()->setCellValue("U1", "upc");
        
        //Attributes
        $objWorkSheet = $phpEx->createSheet(4);
        $phpEx->setActiveSheetIndex(4);//5-th sheet is Attributes!
        $phpEx->getActiveSheet()->setTitle('Attributes');
        
        //Set header for Attributes sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "language_id");
        $phpEx->getActiveSheet()->setCellValue("C1", "attribute_group");
        $phpEx->getActiveSheet()->setCellValue("D1", "attribute_name");
        $phpEx->getActiveSheet()->setCellValue("E1", "text");
        
        //Specials
        $objWorkSheet = $phpEx->createSheet(5);
        $phpEx->setActiveSheetIndex(5);//6-th sheet is Specials!
        $phpEx->getActiveSheet()->setTitle('Specials');
        
        //Set header for Specials sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "customer_group");
        $phpEx->getActiveSheet()->setCellValue("C1", "priority");
        $phpEx->getActiveSheet()->setCellValue("D1", "price");
        $phpEx->getActiveSheet()->setCellValue("E1", "date_start");
        $phpEx->getActiveSheet()->setCellValue("F1", "date_end");
        
        //Discounts
        $objWorkSheet = $phpEx->createSheet(6);
        $phpEx->setActiveSheetIndex(6);//7-th sheet is Discounts!
        $phpEx->getActiveSheet()->setTitle('Discounts');
        
        //Set header for Discounts sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "customer_group");
        $phpEx->getActiveSheet()->setCellValue("C1", "quantity");
        $phpEx->getActiveSheet()->setCellValue("D1", "priority");
        $phpEx->getActiveSheet()->setCellValue("E1", "price");
        $phpEx->getActiveSheet()->setCellValue("F1", "date_start");
        $phpEx->getActiveSheet()->setCellValue("G1", "date_end");
        
        //Rewards
        $objWorkSheet = $phpEx->createSheet(7);
        $phpEx->setActiveSheetIndex(7);//8-th sheet is Rewards!
        $phpEx->getActiveSheet()->setTitle('Rewards');
        
        //Set header for Rewards sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "customer_group");
        $phpEx->getActiveSheet()->setCellValue("C1", "points");
       
        //$objWrt = new PHPExcel_Writer_Excel2007($phpEx);
        //$objWrt->save("scrape_Excel_file.xlsx");
}         
                
        //read file starting urls
        $row = 0;
        $x = 0;
        if(($handle = fopen("start_data.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $val[$x] = trim($data[1]);
                $url[$x] = $val[$row];
                ++$x;
                ++$row;
                }// end of while

            fclose($handle);
        }// end of if $handle
        
        
        // opening listing pages with cUrl
        $line = 2;
        foreach($url as $value) {
            set_time_limit(0);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $value);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $data = curl_exec($ch);
            curl_close($ch);
            $html = new simple_html_dom($data);
            $y = 0;
            // Opening details pages with cUrl
            foreach($html->find('#tableSearchResults2 tbody tr') as $entrie) {
                $t[$y] = $entrie->find('td[1] a', 0)->href;
                echo "</br>"."---------------------"."</br>";
                echo "</br>"."Url of product is: $t[$y]"."</br>";
                
                set_time_limit(0);
                $chp = curl_init();
                curl_setopt($chp, CURLOPT_URL, $t[$y]);
                curl_setopt($chp, CURLOPT_HEADER, false);
                curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($chp, CURLOPT_SSL_VERIFYPEER, false);

               $datap = curl_exec($chp);
               curl_close($chp);
               $htmlp = new simple_html_dom($datap);
               // getting data for upload
               $spec_chars = array(chr(145), chr(146), chr(147), chr(147), chr(148), chr(149), chr(150), chr(151), chr(152), chr(153),);
               $h11 = $htmlp->find('#ProductDescription h1', 0)->innertext;
               $h1 = str_replace($spec_chars, '', $h11);
               $h22 = $htmlp->find('#ProductDescription h2', 0)->innertext;
               $name = $h1;
               if (!empty($h22)) {
                   $h2 = str_replace($spec_chars, '', $h22);
                   $name = $h1.", ".$h2; //for upload
               }
               
               
               if (!empty($htmlp->find('#Manufacturer img'))){
                   $m1 = $htmlp->find('#Manufacturer img', 0)->alt; //for upload
                   $lma = strlen($m1);
               } elseif (!empty($htmlp->find('#Manufacturer'))) {
                   $m1 = $htmlp->find('#Manufacturer', 0)->innertext; //for upload
                   $lma = strlen($m1);
               } 
               if ($lma === 5) {
                   $m1 = 'Ortho-Direct';
               }
                echo "DIN 1 LUNGIMEA MANUFACTURER ESTE: $lma"."</br>";

                
               if (!empty($ig_url = $htmlp->find('#ProductImage img', 0)->src)) {
                    $img_url = "http://www.ortho-direct.com/".$ig_url;
               } else {
                   $ig_url = $htmlp->find('#ProductDescription p img', 0)->src;
                   $img_url = "http://www.ortho-direct.com/".$ig_url;
               }
               $text1 = $htmlp->find('#ProductDescription', 0)->innertext;
               $description = preg_replace("/<img[^>]+>/i", "", $text1); //for upload
               $meta_description = strip_tags($description); //for upload
               $mod = preg_match('~Model (.*)~', $h2, $mtch);
               $modl = $mtch[1];
               if (!empty($modl)) {
                   $model = '51-ODP-'.$modl; //for upload
               } else {
                   $model = '51-ODP'; //for upload
               }
                $p_mpn = $model; //for upload
                
               //Only to get visual a bit!
                $iiname = preg_match('~.*\/(.*)~', $img_url,$ima);
                $iname = $ima[1]; //changing name to main image
                $img_file = file_get_contents($img_url);
                $file_loc = (dirname(__FILE__).'/images/'.$iname); //for saving main image
                $file_handler = fopen($file_loc, 'w');
                $image_name = 'data/products/orthodirect/'.$iname; //for upload
                if(fwrite($file_handler, $img_file) == false) {
                    echo "Error downloading image file from $img_url";
                } else {
                    echo "Image saved at images folder!"."</br>";
                    echo "</br>"." ---------------------- "."</br>";
                    echo '<img src ="'.'/images/'.$iname.'" /><br>';
                    echo "</br>"." ---------------------- "."</br>";
                }
                fclose($file_handler);
                
                $pid = preg_match('~prodId=(.*)&~', $t[$y], $mct);
                $pp = $mct[1];
                $pa = strlen($pp);
                if ($pa > 2) {
                    $product_id = "100".$pp; //for upload
                } else {
                    $product_id = "1000".$pp; //for upload
                }
                
                //Manipulating data for fields like SEO_Key, Tags
                $se_key = preg_replace('/[^A-Za-z0-9\-]/', '-', $name);
                $seo_keyword = preg_replace('/-+/', '-', $se_key); //for upload
                $meta_key = $name; //for upload
                $tgs = preg_replace('/[^A-Za-z0-9]/', ',', $name); 
                $tags = preg_replace('/,+/', ',', $tgs); //for upload
                
                //Getting and setting category corespondence
                $ct_id = preg_match('~category=(.*)~', $t[$y], $act);
                $cat_id = $act[1];
                echo "Scrape Category ID is: $cat_id"."</br>";
                //Setting Orthazone Categories
                if ($cat_id === '2') {
                    $category_id = "62,982,978";
                }
                if ($cat_id === "18") {
                    $category_id = "18,951";
                }
                if ($cat_id === '19') {
                    $category_id = "258,952";
                }
                if ($cat_id === "22") {
                    $category_id = "954,983";
                }
                if ($cat_id === '23') {
                    $category_id = "17,955";
                }
                if ($cat_id === "24") {
                    $category_id = "956,29";
                }
                if ($cat_id === '25') {
                    $category_id = "957";
                }
                if ($cat_id === "26") {
                    $category_id = "958,25";
                }
                if ($cat_id === '27') {
                    $category_id = "58,959";
                }
                if ($cat_id === "34") {
                    $category_id = "72,960";
                }
                if ($cat_id === '17') {
                    $category_id = "31,961";
                }
                if ($cat_id === "16") {
                    $category_id = "24,962";
                }
                if ($cat_id === '15') {
                    $category_id = "629,963";
                }
                if ($cat_id === "4") {
                    $category_id = "964,984";
                }
                if ($cat_id === '5') {
                    $category_id = "31,965";
                }
                if ($cat_id === "6") {
                    $category_id = "27,966";
                }
                if ($cat_id === '7') {
                    $category_id = "603,967";
                }
                if ($cat_id === "36") {
                    $category_id = "43,968";
                }
                if ($cat_id === '30') {
                    $category_id = "46,969";
                }
                if ($cat_id === "8") {
                    $category_id = "50,970";
                }
                if ($cat_id === '9') {
                    $category_id = "608,971";
                }
                if ($cat_id === "10") {
                    $category_id = "233,972";
                }
                if ($cat_id === '11') {
                    $category_id = "262,979";
                }
                if ($cat_id === "12") {
                    $category_id = "498,973";
                }
                if ($cat_id === '13') {
                    $category_id = "167,974";
                }
                if ($cat_id === "29") {
                    $category_id = "825,976";
                }
                 echo "Upload Category ID is: $category_id"."</br>";
                 
                 
                //getting the available options
                $z = 0;
                foreach ($htmlp->find('#addToCart table tbody tr') as $row) {
                    $drow[$z] = $row->innertext;
                    $mpn[$z] = $row->find('th', 0)->innertext;
                    $anteprice[$z] = preg_match('~\$(\d+.\d+)~', $drow[$z], $ppr);
                    $price[$z] = $ppr[1];
                    $fprice = array();
                    if(!empty($price[$z])) {
                        array_push($fprice, $price[$z]);
                        echo "<pre> Option ARRAY Price is: "; print_r($fprice); echo "</pre>";
                    }
                    //$con[$z] = $row->find('td[1]', 0)->innertext;
                    //Start checking if there are more option fields...
                    //$can[$z] = $row->find('td[2]', 0)->innertext;
                    //$cen[$z] = $row->find('td[3]', 0)->innertext;
                    //$cin[$z] = $row->find('td[4]', 0)->innertext;
                    echo "Option MPN[$z] is: $mpn[$z]"."</br>";
                    echo "Option Price[$z] is: $price[$z]"."</br>";
                    
                    //echo "Option content3 is: $cen[$z]"."</br>";
                    //echo "Option content4 is: $cin[$z]"."</br>";
                    echo "THE row $z contains this data: $drow[$z]"."</br>";
                    ++$z;
                }
                
                //dumping data to Products sheet
{              $phpEx->setActiveSheetIndex(1);//second sheet is Products!
               $phpEx->getActiveSheet()->setCellValue("A".$line, "$product_id");
               $phpEx->getActiveSheet()->setCellValue("B".$line, "$name");
               $phpEx->getActiveSheet()->setCellValue("C".$line, $category_id);
               $phpEx->getActiveSheet()->setCellValue("D".$line, "");
               $phpEx->getActiveSheet()->setCellValue("E".$line, "");
               $phpEx->getActiveSheet()->setCellValue("F".$line, "");
               $phpEx->getActiveSheet()->setCellValue("G".$line, "");
               $phpEx->getActiveSheet()->setCellValue("H".$line, "");
               $phpEx->getActiveSheet()->setCellValue("I".$line, "$p_mpn");
               $phpEx->getActiveSheet()->setCellValue("J".$line, "");
               $phpEx->getActiveSheet()->setCellValue("K".$line, qty);
               $phpEx->getActiveSheet()->setCellValue("L".$line, "$model");
               $phpEx->getActiveSheet()->setCellValue("M".$line, "$m1");
               $phpEx->getActiveSheet()->setCellValue("N".$line, "$image_name");
               $phpEx->getActiveSheet()->setCellValue("O".$line, "yes");
               $phpEx->getActiveSheet()->setCellValue("P".$line, "Pprice");
               $phpEx->getActiveSheet()->setCellValue("Q".$line, points);
               $phpEx->getActiveSheet()->setCellValue("R".$line, "$date_added");
               $phpEx->getActiveSheet()->setCellValue("S".$line, "$date_added");
               $phpEx->getActiveSheet()->setCellValue("T".$line, "$date_added");
               $phpEx->getActiveSheet()->setCellValue("U".$line, weight);
               $phpEx->getActiveSheet()->setCellValue("V".$line, "lb");
               $phpEx->getActiveSheet()->setCellValue("W".$line, "1");
               $phpEx->getActiveSheet()->setCellValue("X".$line, "1");
               $phpEx->getActiveSheet()->setCellValue("Y".$line, "1");
               $phpEx->getActiveSheet()->setCellValue("Z".$line, "in");
               $phpEx->getActiveSheet()->setCellValue("AA".$line, "true");
               $phpEx->getActiveSheet()->setCellValue("AB".$line, "9");
               $phpEx->getActiveSheet()->setCellValue("AC".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AD".$line, lang);
               $phpEx->getActiveSheet()->setCellValue("AE".$line, "$seo_keyword");
               $phpEx->getActiveSheet()->setCellValue("AF".$line, "$description");
               $phpEx->getActiveSheet()->setCellValue("AG".$line, "$meta_description");
               $phpEx->getActiveSheet()->setCellValue("AH".$line, "$meta_key");
               $phpEx->getActiveSheet()->setCellValue("AI".$line, stock_status_id);
               $phpEx->getActiveSheet()->setCellValue("AJ".$line, store_ids);
               $phpEx->getActiveSheet()->setCellValue("AK".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AL".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AM".$line, "$tags");
               $phpEx->getActiveSheet()->setCellValue("AN".$line, "0");
               $phpEx->getActiveSheet()->setCellValue("AO".$line, "true");
               $phpEx->getActiveSheet()->setCellValue("AP".$line, "1");
               $phpEx->getActiveSheet()->setCellValue("AQ".$line, supplier_id);
               $phpEx->getActiveSheet()->setCellValue("AR".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AR".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AR".$line, "");
            
            //dumping data to Options sheet
               /*$phpEx->setActiveSheetIndex(3);//second sheet is Options!
               $phpEx->getActiveSheet()->setCellValue("A".$line, "$prod_id");
               $phpEx->getActiveSheet()->setCellValue("B".$line, lang);
               $phpEx->getActiveSheet()->setCellValue("C".$line, "$option");
               $phpEx->getActiveSheet()->setCellValue("D".$line, "checkbox");
               $phpEx->getActiveSheet()->setCellValue("E".$line, "value");
               $phpEx->getActiveSheet()->setCellValue("F".$line, "");
               $phpEx->getActiveSheet()->setCellValue("G".$line, "true");
               $phpEx->getActiveSheet()->setCellValue("H".$line, qty);
               $phpEx->getActiveSheet()->setCellValue("I".$line, "true");
               $phpEx->getActiveSheet()->setCellValue("J".$line, "$price");
               $phpEx->getActiveSheet()->setCellValue("K".$line, "+");
               $phpEx->getActiveSheet()->setCellValue("L".$line, points);
               $phpEx->getActiveSheet()->setCellValue("M".$line, "+");
               $phpEx->getActiveSheet()->setCellValue("N".$line, "$weight");
               $phpEx->getActiveSheet()->setCellValue("O".$line, "+");
               $phpEx->getActiveSheet()->setCellValue("P".$line, "");
               $phpEx->getActiveSheet()->setCellValue("Q".$line, "");
               $phpEx->getActiveSheet()->setCellValue("R".$line, "");
               $phpEx->getActiveSheet()->setCellValue("S".$line, "");
               $phpEx->getActiveSheet()->setCellValue("T".$line, "$sku");
               $phpEx->getActiveSheet()->setCellValue("U".$line, "");*/
}                     
               
                //for debug reasons
                echo "Title of product is: $name"."</br>";
                echo "Product Id is: $product_id"."</br>";
                echo "Date added is: $date_added"."</br>";
                //echo "Date modified is: $date_modified"."</br>";
                //echo "Date available is: $date_available"."</br>";
                echo "H1 of product is: $h1"."</br>";
                echo "H2 of product is: $h2"."</br>";
                echo "Product manufacturer is: $m1"."</br>";
                echo "Image Url of product is: $img_url"."</br>";
                echo "Description for product is: $description"."</br>";
                echo "Meta_description for product is: $meta_description"."</br>";
                echo "Product MPN is: $p_mpn"."</br>";
                echo "Part Product model is: $modl"."</br>";
                echo "Product model is: $model"."</br>";
                echo "Seo Keyword is: $seo_keyword"."</br>";
                echo "Tags are: $tags"."</br>";
                echo "PP are: $pp"."</br>";
                echo "Lungimea este de: $pa"."</br>";
                
                ++$line;
                ++$y;
            }
        }
            
        $objWrt = new PHPExcel_Writer_Excel2007($phpEx);
        $objWrt->save("scrape_OD_file_$date_added.xlsx");    
            
            
        ?>
    </body>
</html>
