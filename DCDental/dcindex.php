<!DOCTYPE html>
<!--
This is the actual DC Dental scraper!
Gathers data following pages and paginations to the 
product details page. The data is supposed to be returned
as an xlsx file with multiple tabs.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>DC Dental Scraper</title>
    </head>
    <body>
        <?php
        require '/start_data.csv'; //start urls
        require 'libs/simple_html_dom/simple_html_dom.php'; //simple html DOM
        require 'libs/Classes/PHPExcel.php'; //excel composer
        
        //Def. constants for excel building
        const lang = "1";
        const supplier_id = "71";
        //for now cat_id are constants!
        const cat_id = "982";
        const qty = '10000';
        const req_shipp = 'yes';
        const points = '0';
        //const weight = '1,00'; This should be taken from scraping!
        const unit = 'lb';
        const length = '1';
        const width = '1';
        const height = '1';
        const length_unit = 'in';
        const status_enabled = 'true';
        const tax_class_id = '9';
        const store_ids = '0,1,4'; //may change!
        const substract = 'true';
        const sort_order = '0';
        const stock_status_id = '7';
        
        $date = date("Y-m-d");
        $datetime = date("Y-m-d H:i:s");

        //Setting Excel file / sheets and headers of sheets
{        
        $phpEx = new PHPExcel();
        
        //Categories
        $objWorkSheet = $phpEx->createSheet(0);    
        $phpEx->setActiveSheetIndex(0);//second sheet is Categories!
        $phpEx->getActiveSheet()->setTitle('Categories');
        
        //Set header for categories sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "category_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "parent_id");
        $phpEx->getActiveSheet()->setCellValue("C1", "name");
        $phpEx->getActiveSheet()->setCellValue("D1", "top");
        $phpEx->getActiveSheet()->setCellValue("E1", "columns");
        $phpEx->getActiveSheet()->setCellValue("F1", "sort_order");
        $phpEx->getActiveSheet()->setCellValue("G1", "image_name");
        $phpEx->getActiveSheet()->setCellValue("H1", "date_added");
        $phpEx->getActiveSheet()->setCellValue("I1", "date_modified");
        $phpEx->getActiveSheet()->setCellValue("J1", "language_id");
        $phpEx->getActiveSheet()->setCellValue("K1", "seo_keyword");
        $phpEx->getActiveSheet()->setCellValue("L1", "description");
        $phpEx->getActiveSheet()->setCellValue("M1", "meta_description");
        $phpEx->getActiveSheet()->setCellValue("N1", "meta_keywords");
        $phpEx->getActiveSheet()->setCellValue("O1", "store_ids");
        $phpEx->getActiveSheet()->setCellValue("P1", "layout");
        $phpEx->getActiveSheet()->setCellValue("Q1", "status enabled");
        
        //Products
        $objWorkSheet = $phpEx->createSheet(1);    
        $phpEx->setActiveSheetIndex(1);//second sheet is Products!
        $phpEx->getActiveSheet()->setTitle('Products');
            
        //Set header for Products sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "name");
        $phpEx->getActiveSheet()->setCellValue("C1", "categories");
        $phpEx->getActiveSheet()->setCellValue("D1", "sku");
        $phpEx->getActiveSheet()->setCellValue("E1", "upc");
        $phpEx->getActiveSheet()->setCellValue("F1", "ean");
        $phpEx->getActiveSheet()->setCellValue("G1", "jan");
        $phpEx->getActiveSheet()->setCellValue("H1", "isbn");
        $phpEx->getActiveSheet()->setCellValue("I1", "mpn");
        $phpEx->getActiveSheet()->setCellValue("J1", "location");
        $phpEx->getActiveSheet()->setCellValue("K1", "quantity");
        $phpEx->getActiveSheet()->setCellValue("L1", "model");
        $phpEx->getActiveSheet()->setCellValue("M1", "manufacturer");
        $phpEx->getActiveSheet()->setCellValue("N1", "image_name");
        $phpEx->getActiveSheet()->setCellValue("O1", "requires shipping");
        $phpEx->getActiveSheet()->setCellValue("P1", "price");
        $phpEx->getActiveSheet()->setCellValue("Q1", "points");
        $phpEx->getActiveSheet()->setCellValue("R1", "date_added");
        $phpEx->getActiveSheet()->setCellValue("S1", "date_modified");
        $phpEx->getActiveSheet()->setCellValue("T1", "date_available");
        $phpEx->getActiveSheet()->setCellValue("U1", "weight");
        $phpEx->getActiveSheet()->setCellValue("V1", "unit");
        $phpEx->getActiveSheet()->setCellValue("W1", "lenght");
        $phpEx->getActiveSheet()->setCellValue("X1", "width");
        $phpEx->getActiveSheet()->setCellValue("Y1", "height");
        $phpEx->getActiveSheet()->setCellValue("Z1", "lenght unit");
        $phpEx->getActiveSheet()->setCellValue("AA1", "status enabled");
        $phpEx->getActiveSheet()->setCellValue("AB1", "tax_class_id");
        $phpEx->getActiveSheet()->setCellValue("AC1", "viewed");
        $phpEx->getActiveSheet()->setCellValue("AD1", "language_id");
        $phpEx->getActiveSheet()->setCellValue("AE1", "seo_keyword");
        $phpEx->getActiveSheet()->setCellValue("AF1", "description");
        $phpEx->getActiveSheet()->setCellValue("AG1", "meta_description");
        $phpEx->getActiveSheet()->setCellValue("AH1", "meta_keywords");
        $phpEx->getActiveSheet()->setCellValue("AI1", "status_id");
        $phpEx->getActiveSheet()->setCellValue("AJ1", "store_ids");
        $phpEx->getActiveSheet()->setCellValue("AK1", "layout");
        $phpEx->getActiveSheet()->setCellValue("AL1", "related_ids");
        $phpEx->getActiveSheet()->setCellValue("AM1", "tags");
        $phpEx->getActiveSheet()->setCellValue("AN1", "sort_order");
        $phpEx->getActiveSheet()->setCellValue("AO1", "subtract");
        $phpEx->getActiveSheet()->setCellValue("AP1", "minimum");
        $phpEx->getActiveSheet()->setCellValue("AQ1", "supplier_id");
        $phpEx->getActiveSheet()->setCellValue("AR1", "cost");
        $phpEx->getActiveSheet()->setCellValue("AS1", "gross_profit_percent");
        $phpEx->getActiveSheet()->setCellValue("AT1", "alternate_id"); //end of header!
        
        //AdditionalImages
        $objWorkSheet = $phpEx->createSheet(2);
        $phpEx->setActiveSheetIndex(2);//third sheet is AdditionalImages!
        $phpEx->getActiveSheet()->setTitle('AdditionalImages');
        
        //Set header for AdditionalImages sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "image");
        $phpEx->getActiveSheet()->setCellValue("C1", "sort_order");
        
        //Options
        $objWorkSheet = $phpEx->createSheet(3);
        $phpEx->setActiveSheetIndex(3);//4-th sheet is Options!
        $phpEx->getActiveSheet()->setTitle('Options');
                
        //Set header for Options sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "language_id");
        $phpEx->getActiveSheet()->setCellValue("C1", "option");
        $phpEx->getActiveSheet()->setCellValue("D1", "type");
        $phpEx->getActiveSheet()->setCellValue("E1", "value");
        $phpEx->getActiveSheet()->setCellValue("F1", "image");
        $phpEx->getActiveSheet()->setCellValue("G1", "required");
        $phpEx->getActiveSheet()->setCellValue("H1", "quantity");
        $phpEx->getActiveSheet()->setCellValue("I1", "substract");
        $phpEx->getActiveSheet()->setCellValue("J1", "price");
        $phpEx->getActiveSheet()->setCellValue("K1", "price prefix");
        $phpEx->getActiveSheet()->setCellValue("L1", "points");
        $phpEx->getActiveSheet()->setCellValue("M1", "points prefix");
        $phpEx->getActiveSheet()->setCellValue("N1", "weight");
        $phpEx->getActiveSheet()->setCellValue("O1", "weight prefix");
        $phpEx->getActiveSheet()->setCellValue("P1", "sort_order");
        $phpEx->getActiveSheet()->setCellValue("Q1", "cost");
        $phpEx->getActiveSheet()->setCellValue("R1", "cost_prefix");
        $phpEx->getActiveSheet()->setCellValue("S1", "sku");
        $phpEx->getActiveSheet()->setCellValue("T1", "mpn");
        $phpEx->getActiveSheet()->setCellValue("U1", "upc");
        
        //Attributes
        $objWorkSheet = $phpEx->createSheet(4);
        $phpEx->setActiveSheetIndex(4);//5-th sheet is Attributes!
        $phpEx->getActiveSheet()->setTitle('Attributes');
        
        //Set header for Attributes sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "language_id");
        $phpEx->getActiveSheet()->setCellValue("C1", "attribute_group");
        $phpEx->getActiveSheet()->setCellValue("D1", "attribute_name");
        $phpEx->getActiveSheet()->setCellValue("E1", "text");
        
        //Specials
        $objWorkSheet = $phpEx->createSheet(5);
        $phpEx->setActiveSheetIndex(5);//6-th sheet is Specials!
        $phpEx->getActiveSheet()->setTitle('Specials');
        
        //Set header for Specials sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "customer_group");
        $phpEx->getActiveSheet()->setCellValue("C1", "priority");
        $phpEx->getActiveSheet()->setCellValue("D1", "price");
        $phpEx->getActiveSheet()->setCellValue("E1", "date_start");
        $phpEx->getActiveSheet()->setCellValue("F1", "date_end");
        
        //Discounts
        $objWorkSheet = $phpEx->createSheet(6);
        $phpEx->setActiveSheetIndex(6);//7-th sheet is Discounts!
        $phpEx->getActiveSheet()->setTitle('Discounts');
        
        //Set header for Discounts sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "customer_group");
        $phpEx->getActiveSheet()->setCellValue("C1", "quantity");
        $phpEx->getActiveSheet()->setCellValue("D1", "priority");
        $phpEx->getActiveSheet()->setCellValue("E1", "price");
        $phpEx->getActiveSheet()->setCellValue("F1", "date_start");
        $phpEx->getActiveSheet()->setCellValue("G1", "date_end");
        
        //Rewards
        $objWorkSheet = $phpEx->createSheet(7);
        $phpEx->setActiveSheetIndex(7);//8-th sheet is Rewards!
        $phpEx->getActiveSheet()->setTitle('Rewards');
        
        //Set header for Rewards sheet
        $phpEx->getActiveSheet()->setCellValue("A1", "product_id");
        $phpEx->getActiveSheet()->setCellValue("B1", "customer_group");
        $phpEx->getActiveSheet()->setCellValue("C1", "points");
       
        //$objWrt = new PHPExcel_Writer_Excel2007($phpEx);
        //$objWrt->save("scrape_Excel_file.xlsx");
}                
        //read file starting urls
        $row = 0;
        $x = 0;
        if(($handle = fopen("start_data.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $val[$x] = trim($data[1]);
                $url[$x] = $val[$row];
                ++$x;
                ++$row;
                }// end of while

            fclose($handle);
        }// end of if $handle
        
        // opening listing pages with cUrl
        $line = 2;
        foreach($url as $value) {
            set_time_limit(0);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $value);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $data = curl_exec($ch);
            curl_close($ch);
            $html = new simple_html_dom($data);
            $y = 0;
            
            // Opening details pages with cUrl
            foreach($html->find('div[class=facets-items-collection-view-cell-span12]') as $entrie) {
                $t[$y] = $entrie->find('a[class=facets-item-cell-list-anchor]', 0)->href;
                $t[$y] = "http://www.dcdental.com".$t[$y];
                $itemd[$y] = $entrie->find('input[class=facets-item-cell-list-add-to-cart-itemid]', 0)->value;
                $prod_id = 1000000 + $itemd[$y];
                echo "</br>"."ITEM ID is: $itemd[$y]"."</br>";
                echo "</br>"."Product: $y"."</br>";
                echo "</br>"."Url of product is: $t[$y]"."</br>";
                echo "</br>"."PRODUCT ID IS: $prod_id"."</br>";
                
                set_time_limit(0);
                $chp = curl_init();
                curl_setopt($chp, CURLOPT_URL, $t[$y]);
                curl_setopt($chp, CURLOPT_HEADER, false);
                curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($chp, CURLOPT_SSL_VERIFYPEER, false);

               $datap = curl_exec($chp);
               curl_close($chp);
               $htmlp = new simple_html_dom($datap);
               
               // getting data for upload
               //Gettin title formated as requested
               $tt1 = $htmlp->find('h1', 0)->innertext;
               if(!empty($tt222 = $htmlp->find('button[data-target=item-details-info-1]', 0))) {
                    $tt22 = $tt222->find('p[class=item-details-info-hint]', 0)->innertext;
                    $t2t2 = preg_match('~Name(.*)Manuf~', $tt22, $resul);
                    $tt2 = $resul[1];
               }
               if(!empty($tt2)) {
                    $title = $tt2.", ".$tt1;
               } else {
                    $title = $tt1;
               }
               
               //Getting manufacturer
               $manuf = $htmlp->find('div[class=item-details-main-content-manufacturer]', 0)->innertext;
               if ($manuf === "House Brand") {
                   $manuf = "DC Dental";
               }
               
               //Getting product_id
               $prod_id = 1000000 + $line;
               
               //Getting SKU of product
               $sku = $htmlp->find('span[class=item-details-sku-value]', 0)->innertext;
               
               //Getting price and format price
               $pprice = $htmlp->find('span[itemprop=price]', 0)->innertext;
               $sol = preg_match('~(\d.+)~', $pprice, $valusd);
               $ppprice = $valusd[1];
               //$price = str_replace('.', ',', $ppprice);
               $price = $ppprice;
               
               //Getting image url and format the image name
               if (!empty($htmlp->find('meta[name=og:image]'))) {
                    $preimg = $htmlp->find('meta[name=og:image]', 0)->content;
                    
               } else {
                    $preimg = $htmlp->find('meta[name=twitter:image:src]')->content;;
               }
               $prmg = preg_match('~(.*)\?~', $preimg, $ip);
               $img_url = $ip[1];
               if (!empty($aaa = preg_match('~images/(.*)~', $img_url, $pina))){
                   $iname = $pina[1];
                   $img_name = "data/products/dcdental/"."$iname";
               } else {
                   $img_name = "data/products/dcdental/no_image_available.jpeg";
               }
               
               //Getting option name
               $option = "Select ".$tt2;
               
               //Getting description as html object for the meta_descr field
               $det1 = $htmlp->find('#item-details-content-container-0 div', 0)->outertext;
               $det2 =$htmlp->find('#item-details-content-container-1', 0)->outertext;
               $description = $det1."</br>".$det2;
               
               //Getting weight for product and formating as number
               $wgt = $htmlp->find('section[class=item-details-more-info-content] button[2] p', 0)->innertext;
               if (!empty(preg_match('~Weight(\d+.\d+)~', $wgt, $mwgt))) {
                    $weight = $mwgt[1];
               } else {
                   $wweight = preg_match('~Weight(.\d+)~', $wgt, $mwgta);
                   $aweight = $mwgta[1];
                   $weight = "0".$aweight;
               }
               if ($weight === '0') {
                   $weight = 1;
               }
               
               //for debuging purposes

               echo "</br>"."---------------------"."</br>";
               echo "</br>"."TITLE of product is: $title"."</br>";
               echo "</br>"."---------------------"."</br>";
               echo "</br>"."Manufacturer of product is: $manuf"."</br>";
               echo "</br>"."---------------------"."</br>";
               echo "</br>"."SKU of product is: $sku"."</br>";
               echo "</br>"."---------------------"."</br>";
               echo "</br>"."Price of product is: $price"."</br>";
               echo "</br>"."---------------------"."</br>";
               echo "</br>"."Img Url of product is: $img_url"."</br>";
               echo "</br>"."---------------------"."</br>";
               echo "</br>"."Description of product is: $description"."</br>";
               echo "</br>"."---------------------"."</br>";
               echo "</br>"."Weight of product is: $weight"."</br>";
               
            //dumping data to Products sheet
               $phpEx->setActiveSheetIndex(1);//second sheet is Products!
               $phpEx->getActiveSheet()->setCellValue("A".$line, "$prod_id");
               $phpEx->getActiveSheet()->setCellValue("B".$line, "$title");
               $phpEx->getActiveSheet()->setCellValue("C".$line, cat_id);
               $phpEx->getActiveSheet()->setCellValue("D".$line, "");
               $phpEx->getActiveSheet()->setCellValue("E".$line, "");
               $phpEx->getActiveSheet()->setCellValue("F".$line, "");
               $phpEx->getActiveSheet()->setCellValue("G".$line, "");
               $phpEx->getActiveSheet()->setCellValue("H".$line, "");
               $phpEx->getActiveSheet()->setCellValue("I".$line, "71-DCD");
               $phpEx->getActiveSheet()->setCellValue("J".$line, "");
               $phpEx->getActiveSheet()->setCellValue("K".$line, qty);
               $phpEx->getActiveSheet()->setCellValue("L".$line, "71-DCD");
               $phpEx->getActiveSheet()->setCellValue("M".$line, "$manuf");
               $phpEx->getActiveSheet()->setCellValue("N".$line, "$img_name");
               $phpEx->getActiveSheet()->setCellValue("O".$line, "yes");
               $phpEx->getActiveSheet()->setCellValue("P".$line, "$price");
               $phpEx->getActiveSheet()->setCellValue("Q".$line, points);
               $phpEx->getActiveSheet()->setCellValue("R".$line, "$datetime");
               $phpEx->getActiveSheet()->setCellValue("S".$line, "$datetime");
               $phpEx->getActiveSheet()->setCellValue("T".$line, "$datetime");
               $phpEx->getActiveSheet()->setCellValue("U".$line, "$weight");
               $phpEx->getActiveSheet()->setCellValue("V".$line, "lb");
               $phpEx->getActiveSheet()->setCellValue("W".$line, "1");
               $phpEx->getActiveSheet()->setCellValue("X".$line, "1");
               $phpEx->getActiveSheet()->setCellValue("Y".$line, "1");
               $phpEx->getActiveSheet()->setCellValue("Z".$line, "in");
               $phpEx->getActiveSheet()->setCellValue("AA".$line, "true");
               $phpEx->getActiveSheet()->setCellValue("AB".$line, "9");
               $phpEx->getActiveSheet()->setCellValue("AC".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AD".$line, lang);
               $phpEx->getActiveSheet()->setCellValue("AE".$line, "seo_key");
               $phpEx->getActiveSheet()->setCellValue("AF".$line, "$description");
               $phpEx->getActiveSheet()->setCellValue("AG".$line, "description");
               $phpEx->getActiveSheet()->setCellValue("AH".$line, "meta_descr");
               $phpEx->getActiveSheet()->setCellValue("AI".$line, stock_status_id);
               $phpEx->getActiveSheet()->setCellValue("AJ".$line, store_ids);
               $phpEx->getActiveSheet()->setCellValue("AK".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AL".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AM".$line, "tags");
               $phpEx->getActiveSheet()->setCellValue("AN".$line, "0");
               $phpEx->getActiveSheet()->setCellValue("AO".$line, "true");
               $phpEx->getActiveSheet()->setCellValue("AP".$line, "1");
               $phpEx->getActiveSheet()->setCellValue("AQ".$line, supplier_id);
               $phpEx->getActiveSheet()->setCellValue("AR".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AR".$line, "");
               $phpEx->getActiveSheet()->setCellValue("AR".$line, "");
            
            //dumping data to Options sheet
               $phpEx->setActiveSheetIndex(3);//second sheet is Options!
               $phpEx->getActiveSheet()->setCellValue("A".$line, "$prod_id");
               $phpEx->getActiveSheet()->setCellValue("B".$line, lang);
               $phpEx->getActiveSheet()->setCellValue("C".$line, "$option");
               $phpEx->getActiveSheet()->setCellValue("D".$line, "checkbox");
               $phpEx->getActiveSheet()->setCellValue("E".$line, "value");
               $phpEx->getActiveSheet()->setCellValue("F".$line, "");
               $phpEx->getActiveSheet()->setCellValue("G".$line, "true");
               $phpEx->getActiveSheet()->setCellValue("H".$line, qty);
               $phpEx->getActiveSheet()->setCellValue("I".$line, "true");
               $phpEx->getActiveSheet()->setCellValue("J".$line, "$price");
               $phpEx->getActiveSheet()->setCellValue("K".$line, "+");
               $phpEx->getActiveSheet()->setCellValue("L".$line, points);
               $phpEx->getActiveSheet()->setCellValue("M".$line, "+");
               $phpEx->getActiveSheet()->setCellValue("N".$line, "$weight");
               $phpEx->getActiveSheet()->setCellValue("O".$line, "+");
               $phpEx->getActiveSheet()->setCellValue("P".$line, "");
               $phpEx->getActiveSheet()->setCellValue("Q".$line, "");
               $phpEx->getActiveSheet()->setCellValue("R".$line, "");
               $phpEx->getActiveSheet()->setCellValue("S".$line, "");
               $phpEx->getActiveSheet()->setCellValue("T".$line, "$sku");
               $phpEx->getActiveSheet()->setCellValue("U".$line, "");
               
               
               
            
            
            
            
            
            
            echo "</br>"."Line is: $line"."</br>";   
            echo "</br>"."---------------------------------------------"."</br>";
            ++$line;
            
            ++$y;   
            }
        }
        $objWrt = new PHPExcel_Writer_Excel2007($phpEx);
        $objWrt->save("scrape_Excel_file_$date.xlsx");
        ?>
    </body>
</html>
