<?php
$config['additionalmarkets'][] = array(
    "name" => "Orthotechnology",
    "title" => "<b>Orthotechnology</b>",
    "url_aliases" => array(
        "www.orthotechnology.com" , 
        "orthotechnology.com",
		"orthotechnology"),
    "fields" => array(
        "title", 
        "description")
    );
?>