<?php
function mspro_dentaurum_title($html){
    $instruction = 'div.product-detail-section h3';
    $parser = new nokogiri($html);
    $data = $parser->get($instruction)->toArray();
    unset($parser);
    if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
        return $data[0]['#text'].'<br>';
    }
return '';
}

function mspro_dentaurum_manufacture($html){
    return 'Dentaurum Inc.';
}



function mspro_dentaurum_description($html){
    $res = '';
    $resa = '';
    $inter = '<p><h3>Product Information</h3></p>';
    $pq = phpQuery::newDocumentHTML($html);
    $text  = $pq->find('#description div.row div.text');

    $res .=$text->html();
    $table = $pq->find('#description div.row div.clearfix');
    $resa .=$table->html();
    $descr = $res . $inter . $resa;
    //echo $descr.'</br>';
    return $descr;
}


function mspro_dentaurum_price($html){
//  $instruction = 'span.pricing';
//  $parser = new nokogiri($html);
//  $data = $parser->get($instruction)->toArray();
//  unset($parser);
//  echo '<pre>'.print_r($data , 1).'</pre>';exit;
//  if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
//  $price = preg_replace("/[^0-9.]/", "",  trim($data[0]['#text']) );
//  return (float) $price;
//  }
return '';	
}


function mspro_dentaurum_special_price($html){
    
//    $instruction = 'span.pricing';
//    $parser = new nokogiri($html);
//    $data = $parser->get($instruction)->toArray();
//    unset($parser);
    //echo '<pre>'.print_r($data , 1).'</pre>';exit;
//    if (isset($data[0]['#text']) && !is_array($data[0]['#text'])) {
//    $special_price = preg_replace("/[^0-9.]/", "",  trim($data[1]['#text']) );
//    return (float) $special_price;
//    }
return '';	
}

function mspro_dentaurum_sku($html){
    return mspro_practicon_model($html);
}

function mspro_dentaurum_model($html){
    $instruction = '#submit_merken';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    unset ($parser);
    if($res){
        $data = $res[0]['onclick'];
        $ares = explode("=", $data);
        $area = explode('&', $ares[2]);
        $res = $area[0];
        return $res;
    }
return '';
}

function mspro_dentaurum_meta_description($html){
    $res =  explode('<meta name="description" content="' , $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
        return $res[0];	
        }
    }
return '';
}

function mspro_dentaurum_meta_keywords($html){
    $res =  explode('<meta name="description" content="' , $html);
    if(count($res) > 1){
        $res = explode('"' , $res[1]);
        if(count($res) > 1){
            $resa = str_replace(array("&nbsp;" , "&amp;" , "-", " ") , array("," , "," , ",", ",") , $res[0]);
            $res = str_replace(array(",,", ",,,", ",,,,"), ",", $resa);
            //echo print_r($res, 1).'<br>';
            return $res;	
        }
    }
return mspro_practicon_title($html);
}


function mspro_dentaurum_main_image($html){
    $arr = mspro_practicon_get_images_arr($html);
    if(isset($arr[0]) && strlen($arr[0]) > 0){
        return $arr[0];
    }
return '';
}


function mspro_practicon_other_images($html){
    $arr = mspro_practicon_get_images_arr($html);
    if(count($arr) > 1){
        unset($arr[0]);
        return $arr;
    }
return array();
}

function mspro_practicon_get_images_arr($html){
    $out = array();
    $instruction = 'a.cloud-zoom img';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    //echo '<pre>'.print_r($res).'</pre>';
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach ($res as $img) {
            $out[] = 'https://shop.dentaurum.us/'.$img['src'];
        }
    $out = array_unique($out);
    //echo '<pre>'.print_r($out, 1).'</pre>';
    return $out; 
    } 
}


function mspro_practicon_options($html){
/*    $out = array();

    $options_NAMES = array();
    $instruction1 = 'div.item-selection-wrapper select';
    $parser1 = new nokogiri($html);
    $res1 = $parser1->get($instruction1)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
	if(isset($res1) && is_array($res1) && count($res1) > 0){
        foreach($res1 as $pos_option_name){
            if(isset($pos_option_name['option']) && count($pos_option_name['option']) > 0){
				$options_NAMES[] = str_replace("-"," ",$pos_option_name['option'][0]['#text']);
            }
        }
    }
	
    $instruction = 'div.item-selection-wrapper select';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    //echo '<pre>'.print_r($res , 1).'</pre>';exit;
    unset($parser);
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $key => $POS_option){
            if(isset($POS_option['option']) && is_array($POS_option['option']) && count($POS_option['option']) > 0){
				array_shift($POS_option['option']);

                $OPTION = array();
                $OPTION['name'] = trim(str_replace( array(":") , array("") , $options_NAMES[$key]));
                $OPTION['type'] = "select";
                $OPTION['required'] = true;
                $OPTION['values'] = array();
                foreach($POS_option['option'] as $option_value){
                    if(isset($option_value['#text']) && !is_array($option_value['#text']) && strlen(trim($option_value['#text'])) > 0 ){
                        $OPTION['values'][] = array('name' => $option_value['#text'] , 'price' => 0);
                    }
                }
                if(count($OPTION['values']) > 0){
                    $out[] = $OPTION;
                }
            }
        }
    }
	//$myfile = fopen("/home/demodenta/public_html/newfile.txt", "w") or die("Unable to open file!");
	//fwrite($myfile, $out);
	//fclose($myfile);
    //echo '<pre>'.print_r($out , 1).'</pre>';exit;
 */  
return '';
}


function mspro_practicon_noMoreAvailable($html){
    return true;
}
