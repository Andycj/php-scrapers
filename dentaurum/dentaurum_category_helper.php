<?php
function parse_category($html  , $task){
    //Takes products urls from listing page through fuction "parse_products"
    //And Return the next pages through function "parse_next_page"
		$out = array();
		$out['products'] = parse_products($html , $task);
		$out['next_page'] = parse_next_page($html , $task);
		return $out;
}
function parse_products($html , $task){
    //Retrieves product urls from listings pages. There are 2 types of listing pages and handles both!
    $out = array();
    $instruction = 'figure.figure-hover-overlay a';
    $parser = new nokogiri($html);
    $res = $parser->get($instruction)->toArray();
    unset($parser);
		 
    if(isset($res) && is_array($res) && count($res) > 0){
        foreach($res as $pos_product){
            if(isset($pos_product['href']) && !is_array($pos_product['href']) && strlen($pos_product['href']) > 0 ){
                $out[] = 'https://shop.dentaurum.us/' . $pos_product['href'];
                $out = array_unique($out);
            }
       }return $out;
   } else {
        $instruction = 'td[data-th=Product] a';
        $parser = new nokogiri($html);
        $res = $parser->get($instruction)->toArray();
        unset($parser);
        if(isset($res) && is_array($res) && count($res) > 0){
            foreach($res as $pos_product){
                if(isset($pos_product['href']) && !is_array($pos_product['href']) && strlen($pos_product['href']) > 0 ){
                    $out[] = 'https://shop.dentaurum.us/' . $pos_product['href'];
                    $out = array_unique($out);
                }
            }return $out;
        }
    }
}
function parse_next_page($html , $task){
    //Finds out if there is pagination on listing page + retrieve the numbers of next pages +
    // + retrieves the main sequence for building next pages urls
    //If no pagination, than return false; if pagination, than returns the next pages urls
    $base = 'https://shop.dentaurum.us/';
    $instruction = '#productcount';
    $parser = new nokogiri($html);
    $count = $parser->get($instruction)->toArray();
    unset($parser);
    if(isset($count) && is_array($count) && count($count) > 0){
        foreach($count as $con){
            if(isset($con['#text']) && !is_array($con['#text']) && strlen($con['#text']) > 0 ){
                $t_no = $con['#text'];
                $pag = (int)ceil($t_no / 12);
                $instr = 'div.block-breadcrumb ul li.active a';
                $parser = new nokogiri($html);
                $m_url = $parser->get($instr)->toArray();
                unset($parser);
                if(isset($m_url) && is_array($m_url) && count($m_url) > 0){
                    foreach($m_url as $e_url){
                        if(isset($e_url['href']) && !is_array($e_url['href']) && strlen($e_url['href']) > 0 ){
                            $query = $base . $e_url['href'];
                            
                        }
                    }
                }
                $queryStr = dentaurum_get_next_page($pag, $query);
                return $queryStr;
            }
        }
    }
    return false;
}
function dentaurum_get_next_page($pag, $queryStr){
    //Based on number of next pages and main sequence of next url pages
    //Builds the correct next urls pages
    if ($pag > 1){
        for ($i=2; $i<=$pag; $i++){
            $next = $queryStr . '&page=' . $i;
            echo '<pre>'.$next.'</pre>';
        return $next;
        }
    }
    return false;
}

